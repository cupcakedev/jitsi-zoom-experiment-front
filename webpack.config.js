const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const isDev = process.env.CONFIG_SETUP === 'dev';

module.exports = {
  mode: isDev ? 'development' : 'production',
  entry: {
    app: path.join(__dirname, 'src', 'index.tsx'),
  },
  devtool: isDev ? 'inline-source-map' : undefined,
  resolve: {
    alias: {
      components: path.resolve(__dirname, 'src/components'),
      contexts: path.resolve(__dirname, 'src/contexts'),
      // api: path.resolve(__dirname, 'src/api'),
      reduxes: path.resolve(__dirname, 'src/reduxes'),
      // webrtc: path.resolve(__dirname, 'src/webrtc'),
      // websockets: path.resolve(__dirname, 'src/websockets'),
      // widgets: path.resolve(__dirname, 'src/widgets'),
      utils: path.resolve(__dirname, 'src/utils'),
      types: path.resolve(__dirname, 'src/types'),
      // styles: path.resolve(__dirname, 'src/styles'),
      // hooks: path.resolve(__dirname, 'src/hooks'),
      // environment: path.resolve(__dirname, 'src/environment'),
      constants: path.resolve(__dirname, 'src/constants'),
      pages: path.resolve(__dirname, 'src/pages'),
      globalStyles: path.resolve(__dirname, 'src/globalStyles'),
      node_modules: path.resolve(__dirname, 'node_modules'),
    },
    extensions: ['.ts', '.tsx', '.js', '.css'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)?$/,
        use: ['ts-loader'],
        include: path.resolve(__dirname, 'src'),
        exclude: '/node_modules/',
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: true,
            },
          },
          'css-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {},
          },
        ],
      }, 
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        use: [{loader: 'url-loader?limit=100000'}],
      },
    ],
  },
  output: {
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: process.env.PUBLIC_PATH ? process.env.PUBLIC_PATH : '/',
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new webpack.ProvidePlugin({
      $: path.resolve(__dirname, 'libs/jquery.min.js'),
      JitsiMeetJS: path.resolve(__dirname, 'libs/lib-jitsi-meet.min.js'),
    }),
    new Dotenv({
      systemvars: true,
      defaults: true, // load '.env.defaults' as the default values if empty.
    }),
    new CopyPlugin({
      patterns:[
        {from:'node_modules/@zoom/videosdk/dist/lib', to:'libs/zoom-libs'}
      ]
    }),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html'),
      favicon: path.join(__dirname, 'src', 'favicon.ico'),
    }),
  ],
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist')
    },
    compress: false,
    port: 9000,
    https: true,
    hot: true,
    historyApiFallback: {
      rewrites: [{ from: /^\/*$/, to: '/' }],
    },
  },
  optimization: {
    minimize: !isDev,
  },
};
