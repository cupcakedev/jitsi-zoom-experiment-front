export interface WSMessage<T = any> {
  type: WebsocketMessageTypes;
  data: T;
}

export enum WebsocketMessageTypes {
  HELLO_WORLD = 'hello_world',
  HELLO_THERE = 'hello_there',
  CONNECT_USER = 'connect_user',
  USER_CONNECTED = 'user_connected',
  CREATE_ROOM = 'create_room',
  ROOM_CREATED = 'room_created',
  ROOM_USER_JOIN = 'room_user_join',
  PLAYER_CONNECTED = 'player_connected',
  ROOM_FULL_DATA = 'room_full_data',
  PLAYER_DISCONNECTED = 'player_disconnected',
  SET_AUDIO_MUTE = 'set_audio_mute',
  SET_VIDEO_MUTE = 'set_video_mute',
  AUDIO_CHANGED = 'audio_changed',
  VIDEO_CHANGED = 'video_changed',
  GET_ROOM_DATA = 'get_room_data',
}

export enum WebsocketsActions {
  WS_SEND_MESSAGE = 'ws_send_message',
}
