import { WebsocketsActions } from './websockets.type';

export const wsSendMessage = (data: any) => ({
  type: WebsocketsActions.WS_SEND_MESSAGE,
  payload: {
    data,
  },
});
