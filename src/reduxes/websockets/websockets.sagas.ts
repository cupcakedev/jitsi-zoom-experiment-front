import { takeEvery, call } from 'redux-saga/effects';
import { WebsocketsActions } from './websockets.type';
import { sendMessage } from '../websockets';
import { wsSendMessage } from './websocket.actions';

function* handleWsSendMessage({ payload }: ReturnType<typeof wsSendMessage>) {
  yield call(sendMessage, payload.data);
}

export default function* websocketsFlow() {
  yield takeEvery(WebsocketsActions.WS_SEND_MESSAGE, handleWsSendMessage);
}
