import { fork } from 'redux-saga/effects';
import baseFlow from './base/base.saga';
import conferenceFlow from './conference/conference.sagas';
import jitsiSagas from './jitsi/jitsi.saga';
import websocketsFlow from './websockets/websockets.sagas';
import zoomFlow from './zoom/zoom.sagas';

export default function* rootSaga() {
  yield fork(baseFlow);
  yield fork(jitsiSagas);
  yield fork(websocketsFlow);
  yield fork(conferenceFlow);
  yield fork(zoomFlow);
}
