import { Participant } from '@zoom/videosdk';
import { TrackTypes } from 'reduxes/conference/conference.types';
import { ZoomActionTypes, MediaStream } from './zoom.types';

export const initialize = () => ({
  type: ZoomActionTypes.INITIALIZE,
});

export const connect = (conferenceName: string, JID: string) => ({
  type: ZoomActionTypes.CONNECT,
  payload: { conferenceName, JID },
});

export const connected = (conferenceName: string, JID: string) => ({
  type: ZoomActionTypes.CONNECTED,
  payload: { conferenceName, JID },
});

export const setParticipants = (participants: Participant[]) => ({
  type: ZoomActionTypes.SET_PARTICIPANTS,
  payload: participants,
});

export const updateStream = (stream: MediaStream) => ({
  type: ZoomActionTypes.UPDATE_STREAM,
  payload: stream,
});

export const conferenceIsLoaded = (isLoaded: boolean) => ({
  type: ZoomActionTypes.CONFERENCE_IS_LOADED,
  payload: isLoaded,
});

export const updateParticipant = (
  userId: Participant['userId'],
  update: Partial<Participant>,
) => ({
  type: ZoomActionTypes.UPDATE_PARTICIPANT,
  payload: { userId, update },
});

export const leave = () => ({
  type: ZoomActionTypes.LEAVE,
});

export const muteTrack = (trackType: TrackTypes, isLocal?: boolean) => ({
  type: ZoomActionTypes.MUTE,
  payload: { trackType, isLocal },
});

export const mutedTrack = (trackType: TrackTypes) => ({
  type: ZoomActionTypes.MUTED,
  payload: trackType,
});

export const unmuteTrack = (trackType: TrackTypes, isLocal?: boolean) => ({
  type: ZoomActionTypes.UNMUTE,
  payload: { trackType, isLocal },
});

export const unmutedTrack = (trackType: TrackTypes) => ({
  type: ZoomActionTypes.UNMUTED,
  payload: trackType,
});

export const removeParticipant = (userId: Participant['userId']) => ({
  type: ZoomActionTypes.REMOVE_PARTICIPANT,
  payload: { userId },
});
