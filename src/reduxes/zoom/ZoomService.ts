import { KJUR } from 'jsrsasign';
import ZoomVideo, { ConnectionState } from '@zoom/videosdk';
import { ZoomAllActions, ZoomClient } from './zoom.types';
import * as zoomActions from './zoom.actions';
import { TrackTypes } from 'reduxes/conference/conference.types';

export const conferenceConfig = {
  sdkKey: '29I0hcjiHrzXNjHo4U5CmHuAZIhFmqQ5RK1n',
  sdkSecret: 'wGIAsPqcHXNvGiIleIX1lL92YKWFgWuj2JPZ',
  topic: '', //ConferenceName
  name: '', //Here we use JID
  signature: '',
};

export class ZoomService {
  client: ZoomClient;

  private emitter?: (event: ZoomAllActions) => void;

  private emitAction(event: ZoomAllActions) {
    this.emitter && this.emitter(event);
  }

  public setActionEmitter(emitter: (event: ZoomAllActions) => void) {
    this.emitter = emitter;
  }

  public generateInstantToken(
    appKey: string,
    apiSecret: string,
    topic: string,
    passWord = '',
  ): string {
    let signature = '';
    const iat = Math.round(new Date().getTime() / 1000);
    const exp = iat + 60 * 60 * 2;
    // Header
    const oHeader = { alg: 'HS256', typ: 'JWT' };
    // Payload
    const oPayload = {
      app_key: appKey,
      iat,
      exp,
      tpc: topic,
      pwd: passWord,
    };
    // Sign JWT
    const sHeader = JSON.stringify(oHeader);
    const sPayload = JSON.stringify(oPayload);
    signature = KJUR.jws.JWS.sign('HS256', sHeader, sPayload, apiSecret);
    return signature;
  }

  public async initialize() {
    this.client = ZoomVideo.createClient();
    await this.client.init('en-US', `${window.location.origin}/libs/zoom-libs`);
    console.log('Zoom initialize');
    this.client.on('connection-change', async (payload) => {
      if (payload.state === ConnectionState.Connected) {
        console.log('User connected');
        // await this.startVideoStream();
        this.startAudioStream();
        this.emitAction(zoomActions.conferenceIsLoaded(true));
      }
      if (payload.state === ConnectionState.Closed) {
        console.log('connection closed');
      }
    });
    this.client.on('user-added', (payload) => {
      payload.forEach((item) => {
        console.log('participant %s joins the session', item.displayName);
      });
      // get latest participants list
      const users = this.client.getAllUser();
      this.emitAction(zoomActions.setParticipants(users));
      this.startAudioStream();
      this.updateMediaStream();
    });
    this.client.on('user-updated', async (payload) => {
      payload.forEach((item) => {
        this.emitAction(zoomActions.updateParticipant(item.userId, item));
      });
    });
    this.client.on('user-removed', (payload) => {
      payload.forEach((item) => {
        this.emitAction(zoomActions.removeParticipant(item.userId));
      });
    });
    this.client.on('peer-video-state-change', (payload) => {
      console.log('AAAAA: ', payload);
    });
    this.client.on('peer-video-state-change', (payload) => {
      console.log(payload);
    });
    this.client.on('share-content-dimension-change', (payload) => {
      console.log(payload);
    });
    this.client.on('auto-play-audio-failed', async () => {
      console.log('auto play failed, waiting user interaction');
      await this.startAudioStream();
    });
  }

  public async connect(conferenceName: string, JID: string) {
    const signature = this.generateInstantToken(
      conferenceConfig.sdkKey,
      conferenceConfig.sdkSecret,
      conferenceName,
    );
    await this.client.join(conferenceName, signature, JID);
    this.emitAction(zoomActions.connected(conferenceName, JID));
  }

  leave() {
    this.client.leave();
  }

  async startVideoStream(trackType: TrackTypes) {
    const stream = this.client.getMediaStream();
    await stream.startVideo();
    console.log('video started');
    this.emitAction(zoomActions.unmutedTrack(trackType));
  }

  async stopVideoStream(trackType: TrackTypes) {
    const stream = this.client.getMediaStream();
    await stream.stopVideo();
    console.log('video stoped');
    this.emitAction(zoomActions.mutedTrack(trackType));
  }

  async startAudioStream() {
    const stream = this.client.getMediaStream();
    await stream.startAudio();
    console.log('audio started');
  }

  async updateMediaStream() {
    const stream = this.client.getMediaStream();
    this.emitAction(zoomActions.updateStream(stream));
  }

  async muteAudio(trackType: TrackTypes) {
    const stream = this.client.getMediaStream();
    stream.muteAudio();
    this.emitAction(zoomActions.mutedTrack(trackType));
  }

  async unmuteAudio(trackType: TrackTypes) {
    const stream = this.client.getMediaStream();
    stream.unmuteAudio();
    this.emitAction(zoomActions.unmutedTrack(trackType));
  }
}
