import { Participant, VideoClient, Stream } from '@zoom/videosdk';
import * as zoomActions from './zoom.actions';

export enum ZoomActionTypes {
  INITIALIZE = '[ZOOM<-] Initialize zoom',
  CONNECT = '[ZOOM<-] Connect zoom',
  SET_PARTICIPANTS = '[ZOOM<-] Set participants',
  CONNECTED = '[ZOOM<-] Connected zoom',
  UPDATE_STREAM = '[ZOOM] Update meadia stream',
  CONFERENCE_IS_LOADED = '[ZOOM] Conference is loaded',
  UPDATE_PARTICIPANT = '[ZOOM] Update participant',
  REMOVE_PARTICIPANT = '[ZOOM] Remove participant',
  LEAVE = '[ZOOM] Leave zoom',
  MUTE = '[ZOOM] Mute',
  UNMUTE = '[ZOOM] Unmute',
  MUTED = '[ZOOM] Muted',
  UNMUTED = '[ZOOM] Unmuted',
}

export interface ZoomState {
  stream: MediaStream;
  isInitialized: boolean;
  participants: Participant[];
}

export type ZoomAllActions =
  | ReturnType<typeof zoomActions.initialize>
  | ReturnType<typeof zoomActions.connect>
  | ReturnType<typeof zoomActions.setParticipants>
  | ReturnType<typeof zoomActions.connected>
  | ReturnType<typeof zoomActions.conferenceIsLoaded>
  | ReturnType<typeof zoomActions.updateParticipant>
  | ReturnType<typeof zoomActions.removeParticipant>
  | ReturnType<typeof zoomActions.mutedTrack>
  | ReturnType<typeof zoomActions.unmutedTrack>;

export type ZoomHandler<T = undefined> = (
  state: ZoomState,
  action: T extends (...args: any[]) => infer R ? R : any,
) => ZoomState | void;

export type ZoomClient = typeof VideoClient;
export type MediaStream = typeof Stream;
