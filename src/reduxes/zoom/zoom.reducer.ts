import { ZoomState, ZoomActionTypes, ZoomHandler } from './zoom.types';
import * as zoomActions from './zoom.actions';
import { createReducer } from '@reduxjs/toolkit';

const initialState: ZoomState = {
  stream: null,
  isInitialized: false,
  participants: [],
};

const initializeHandler: ZoomHandler<typeof zoomActions.initialize> = (
  state: ZoomState,
) => ({
  ...state,
  isInitialized: true,
});

const setParticipantsHandler: ZoomHandler<typeof zoomActions.setParticipants> =
  (state: ZoomState, { payload }) => ({
    ...state,
    participants: payload,
  });

const updateMediaStreamHandler: ZoomHandler<typeof zoomActions.updateStream> = (
  state: ZoomState,
  { payload },
) => ({
  ...state,
  stream: payload,
});

const updateParticipantHandler: ZoomHandler<
  typeof zoomActions.updateParticipant
> = (state: ZoomState, { payload }) => ({
  ...state,
  participants: state.participants.map((participant) => {
    if (participant.userId === payload.userId) {
      return { ...participant, ...payload.update };
    }
    return participant;
  }),
});

const removeParticipantHandler: ZoomHandler<
  typeof zoomActions.removeParticipant
> = (state, { payload }) => ({
  ...state,
  participants: state.participants.filter(
    (item) => item.userId !== payload.userId,
  ),
});

const handlers = {
  [ZoomActionTypes.INITIALIZE]: initializeHandler,
  [ZoomActionTypes.SET_PARTICIPANTS]: setParticipantsHandler,
  [ZoomActionTypes.UPDATE_STREAM]: updateMediaStreamHandler,
  [ZoomActionTypes.UPDATE_PARTICIPANT]: updateParticipantHandler,
  [ZoomActionTypes.REMOVE_PARTICIPANT]: removeParticipantHandler,
};

export default createReducer(initialState, handlers);
