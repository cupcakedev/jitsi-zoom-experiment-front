import { call, put, select, takeEvery, fork, take } from 'redux-saga/effects';
import { buffers, eventChannel } from 'redux-saga';
import * as zoomActions from './zoom.actions';
import * as conferenceActions from '../conference/conference.actions';
import { ZoomActionTypes } from './zoom.types';
import { ZoomService } from './ZoomService';
import { TrackTypes } from 'reduxes/conference/conference.types';
import { wsSendMessage } from 'reduxes/websockets/websocket.actions';
import { WebsocketMessageTypes } from 'reduxes/websockets/websockets.type';
import {
  selectConferenceName,
  selectMyJid,
} from 'reduxes/conference/conference.selectors';
import { MdTrendingUp } from 'react-icons/md';

const zoomService: ZoomService = new ZoomService();

const initZoomChannel = (zoomService: ZoomService) => {
  return eventChannel((emitter) => {
    zoomService.setActionEmitter(emitter);
    return () => {
      console.log('disconnect');
    };
  }, buffers.fixed(100));
};

function* initializeSaga() {
  yield call([zoomService, 'initialize']);
}

function* connectionSaga({ payload }: ReturnType<typeof zoomActions.connect>) {
  yield call([zoomService, 'connect'], payload.conferenceName, payload.JID);
}

function* connectedSaga({ payload }: ReturnType<typeof zoomActions.connected>) {
  yield put(
    conferenceActions.isSuccessfullJoined(
      true,
      payload.conferenceName,
      payload.JID,
    ),
  );
}

function* muteSaga({ payload }: ReturnType<typeof zoomActions.muteTrack>) {
  const myJid: string = yield select(selectMyJid);
  const roomName = yield select(selectConferenceName);

  if (payload.trackType === TrackTypes.audio) {
    yield call([zoomService, 'muteAudio'], payload.trackType);
  } else {
    if (payload.isLocal) {
      yield put(
        wsSendMessage({
          type: WebsocketMessageTypes.SET_VIDEO_MUTE,
          data: { roomName, JID: myJid, status: false },
        }),
      );
    } else {
      yield call([zoomService, 'stopVideoStream'], payload.trackType);
    }
  }
}

function* unmuteSaga({ payload }: ReturnType<typeof zoomActions.unmuteTrack>) {
  const myJid: string = yield select(selectMyJid);
  const roomName = yield select(selectConferenceName);

  if (payload.trackType === TrackTypes.audio) {
    yield call([zoomService, 'unmuteAudio'], payload.trackType);
  } else {
    if (payload.isLocal) {
      yield put(
        wsSendMessage({
          type: WebsocketMessageTypes.SET_VIDEO_MUTE,
          data: { roomName, JID: myJid, status: true },
        }),
      );
    } else {
      yield call([zoomService, 'startVideoStream'], payload.trackType);
    }
  }
}

function* mutedSaga({ payload }: ReturnType<typeof zoomActions.mutedTrack>) {
  const roomName = yield select(selectConferenceName);
  const jid = yield select(selectMyJid);
  if (payload === 'audio') {
    yield put(
      wsSendMessage({
        type: WebsocketMessageTypes.SET_AUDIO_MUTE,
        data: { roomName, JID: jid, status: false },
      }),
    );
  } else {
    yield put(
      wsSendMessage({
        type: WebsocketMessageTypes.SET_VIDEO_MUTE,
        data: { roomName, JID: jid, status: false },
      }),
    );
  }
}

function* unmutedSaga({
  payload,
}: ReturnType<typeof zoomActions.unmutedTrack>) {
  const roomName = yield select(selectConferenceName);
  const jid = yield select(selectMyJid);
  if (payload === 'audio') {
    yield put(
      wsSendMessage({
        type: WebsocketMessageTypes.SET_AUDIO_MUTE,
        data: { roomName, JID: jid, status: true },
      }),
    );
  } else {
    yield put(
      wsSendMessage({
        type: WebsocketMessageTypes.SET_VIDEO_MUTE,
        data: { roomName, JID: jid, status: true },
      }),
    );
  }
}

function* disconnectedSaga() {
  yield call([zoomService, 'leave']);
}

export default function* zoomFlow() {
  yield takeEvery(ZoomActionTypes.INITIALIZE, initializeSaga);
  yield takeEvery(ZoomActionTypes.CONNECT, connectionSaga);
  yield takeEvery(ZoomActionTypes.CONNECTED, connectedSaga);
  yield takeEvery(ZoomActionTypes.LEAVE, disconnectedSaga);
  yield takeEvery(ZoomActionTypes.MUTE, muteSaga);
  yield takeEvery(ZoomActionTypes.UNMUTE, unmuteSaga);
  yield takeEvery(ZoomActionTypes.MUTED, mutedSaga);
  yield takeEvery(ZoomActionTypes.UNMUTED, unmutedSaga);

  const channel = yield call(initZoomChannel, zoomService);

  while (true) {
    const action = yield take(channel);
    yield put(action);
  }
}
