import { AppState } from 'types';
import { BaseState } from './base.types';

export const selectBase = (state: AppState): BaseState => state.base;

export const selectHello = (state: AppState): BaseState['hello'] =>
  state.base.hello;

export const selectUUID = (state: AppState): BaseState['UUID'] =>
  state.base.UUID;

export const selectCameraDeviceId = (
  state: AppState,
): BaseState['cameraDeviceId'] => state.base.cameraDeviceId;

export const selectMicDeviceId = (state: AppState): BaseState['micDeviceId'] =>
  state.base.micDeviceId;

export const selectName = (state: AppState): BaseState['name'] =>
  state.base.name;
