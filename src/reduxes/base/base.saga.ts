import { takeEvery, put } from 'redux-saga/effects';
import { WebsocketMessageTypes } from 'reduxes/websockets/websockets.type';
import { BaseActions } from './base.types';
import { wsSendMessage } from '../websockets/websocket.actions';
import { setHello, setUUID } from './base.actions';

function* wsConnectedSaga() {
  console.log('WS user init from wsConnected');
  yield put(
    wsSendMessage({
      type: WebsocketMessageTypes.CONNECT_USER,
      data: {},
    }),
  );
}

function* sayHelloSaga() {
  yield put(
    wsSendMessage({
      type: WebsocketMessageTypes.HELLO_WORLD,
      data: {},
    }),
  );
}

function* helloThereHandler(data) {
  yield put(setHello(data.data.msg));
}

function* userConnectedSaga(data) {
  yield put(setUUID(data.data.UUID));
}

export default function* baseFlow() {
  yield takeEvery(BaseActions.WEB_SOCKET_CONNECTED, wsConnectedSaga);
  yield takeEvery(WebsocketMessageTypes.USER_CONNECTED, userConnectedSaga);
  yield takeEvery(BaseActions.SAY_HELLO, sayHelloSaga);
  yield takeEvery(WebsocketMessageTypes.HELLO_THERE, helloThereHandler);
}
