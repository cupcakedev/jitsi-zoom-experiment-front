import { BaseActions } from './base.types';

export const sayHello = () => ({
  type: BaseActions.SAY_HELLO,
});

export const setVideoDeviceId = (videoDevice: string | null) => ({
  type: BaseActions.SET_VIDEO_DEVICE,
  payload: videoDevice,
});

export const setAudioDeviceId = (audioDevice: string | null) => ({
  type: BaseActions.SET_AUDIO_DEVICE,
  payload: audioDevice,
});

export const setHello = (text: string) => ({
  type: BaseActions.SET_HELLO,
  payload: text,
});

export const setUUID = (UUID: string) => ({
  type: BaseActions.SET_UUID,
  payload: UUID,
});

export const wsConnected = () => ({
  type: BaseActions.WEB_SOCKET_CONNECTED,
});

export const wsDisconnected = () => ({
  type: BaseActions.WEB_SOCKET_DISCONNECTED,
});

export const setName = (name: string) => ({
  type: BaseActions.SET_NAME,
  payload: name,
});

export const setMyJid = (myJID: string) => ({
  type: BaseActions.SET_MY_JID,
  payload: myJID,
});
