import { createReducer } from '@reduxjs/toolkit';
import { setHello, setMyJid, setName, setUUID } from './base.actions';
import { BaseActions, BaseState, BaseHandler } from './base.types';

const initialState: BaseState = {
  hello: '',
  UUID: '',
  myJID: '',
  cameraDeviceId: null,
  micDeviceId: null,
  name: '',
};

const setHelloHandler: BaseHandler<typeof setHello> = (state, { payload }) => ({
  ...state,
  hello: payload,
});

const setUUIDHandler: BaseHandler<typeof setUUID> = (state, { payload }) => ({
  ...state,
  UUID: payload,
});

const setNameHandler: BaseHandler<typeof setName> = (state, { payload }) => ({
  ...state,
  name: payload,
});

const setMyJIDHandler: BaseHandler<typeof setMyJid> = (state, { payload }) => ({
  ...state,
  myJID: payload,
});

const handlers = {
  [BaseActions.SET_HELLO]: setHelloHandler,
  [BaseActions.SET_UUID]: setUUIDHandler,
  [BaseActions.SET_NAME]: setNameHandler,
  [BaseActions.SET_MY_JID]: setMyJIDHandler,
};

export default createReducer(initialState, handlers);
