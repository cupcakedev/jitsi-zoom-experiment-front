export interface BaseState {
  hello: string;
  UUID: string;
  cameraDeviceId: string;
  micDeviceId: string;
  name?: string;
  myJID: string;
}

export enum BaseActions {
  WEB_SOCKET_CONNECTED = 'WEB_SOCKET_CONNECTED',
  SAY_HELLO = 'SAY_HELLO',
  SET_HELLO = 'SET_HELLO',
  SET_UUID = 'SET_UUID',
  SET_VIDEO_DEVICE = 'SET_VIDEO_DEVICE',
  SET_AUDIO_DEVICE = 'SET_AUDIO_DEVICE',
  SET_RTC = 'SET_RTC',
  SET_NAME = 'SET_NAME',
  WEB_SOCKET_DISCONNECTED = 'WEB_SOCKET_DISCONNECTED',
  SET_MY_JID = 'SET_MY_JID',
}

export type BaseHandler<T = undefined> = (
  state: BaseState,
  payload: T extends (...args: any[]) => infer R ? R : any,
) => BaseState | void;
