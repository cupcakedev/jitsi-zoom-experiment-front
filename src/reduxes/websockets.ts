import { eventChannel } from 'redux-saga';
import { call, take, put } from 'redux-saga/effects';
import { WSMessage } from './websockets/websockets.type';
import { wsConnected } from './base/base.actions';

const WEBSOCKET_URL = 'wss://beta-wss.ludio.gg/';
// const WEBSOCKET_URL = 'ws://localhost:1337/';

let ws = new WebSocket(WEBSOCKET_URL);
let reconnectsCount = 0;

export const sendMessage = (data: WSMessage<any>): void => {
  ws.send(JSON.stringify(data));
};

const createAction = (message: WSMessage) => ({
  type: message.type,
  data: message.data,
});

const initWebsocketChannel = () => {
  return eventChannel((emitter) => {
    const connect = () => {
      console.log('try to connect');
      if (reconnectsCount) {
        ws = new WebSocket(WEBSOCKET_URL);
      }

      ws.onopen = () => {
        emitter(wsConnected());
      };

      ws.onmessage = (e) => {
        const message: WSMessage<any> = JSON.parse(e.data);

        return emitter(createAction(message));
      };

      ws.onerror = (err) => {
        const message = `Error ${err.type}`;
        console.log(message);
        ws.close();
      };

      ws.onclose = (e) => {
        console.log(
          'Socket is closed. Reconnect will be attempted in 1 second.',
          e.reason,
        );
        setTimeout(connect, 1000);
        reconnectsCount += 1;
      };
    };

    connect();

    return () => {
      console.log('disconnect');
    };
  });
};

export default function* websocketSagas() {
  const channel = yield call(initWebsocketChannel);

  while (true) {
    const action = yield take(channel);
    yield put(action);
  }
}
