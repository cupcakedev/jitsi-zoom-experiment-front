import { createStore, applyMiddleware, Store, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './rootSaga';
import websocketSagas from './websockets';
import rootReducer from './rootReducer';
import { AppState } from 'types';
import { initialize as initiJitsi } from './jitsi/jitsi.actions';
import { initialize as initZoom } from './zoom/zoom.actions';

declare const window: any;

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        name: 'Warp Module',
        stateSanitizer: (state: AppState) => {
          /*state.players = state.players.map((player: Player) => {
        const {audioTrack, videoTrack, ...rest} = player;
        return rest;
      });*/
          const newState = {
            ...state,
            jitsi: { ...state.jitsi, participants: undefined },
          };
          // const participants =  { ...state.jitsi.participants }
          return newState;
        },
      })
    : compose;

function logger({ getState }) {
  return (next) => (action) => {
    console.log('will dispatch', action);
    const returnValue = next(action);
    console.log('state after dispatch', getState());
    return returnValue;
  };
}

export function makeStore() {
  const sagaMiddleware = createSagaMiddleware();
  const websocketMiddleware = createSagaMiddleware();

  const store: Store<AppState> = createStore(
    rootReducer,
    composeEnhancers(
      applyMiddleware(sagaMiddleware, websocketMiddleware, logger),
    ),
  );

  sagaMiddleware.run(rootSaga);
  websocketMiddleware.run(websocketSagas);

  store.dispatch(initiJitsi());
  store.dispatch(initZoom());
  return store;
}
