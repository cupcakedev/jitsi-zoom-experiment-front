import { put, select, takeEvery } from 'redux-saga/effects';
import { wsSendMessage } from 'reduxes/websockets/websocket.actions';
import { WebsocketMessageTypes } from 'reduxes/websockets/websockets.type';
import { ConferenceActions } from './conference.types';
import {
  selectRTC,
  selectMyJid,
  selectPlayerMuted,
  selectConferenceName,
} from './conference.selectors';
import * as conferenceActions from './conference.actions';
import * as jitsiActions from '../jitsi/jitsi.actions';
import * as zoomActions from '../zoom/zoom.actions';
import * as baseActions from '../base/base.actions';
import { selectName, selectUUID } from 'reduxes/base/base.selector';
import uniqid from 'uniqid';
import { ZoomActionTypes } from 'reduxes/zoom/zoom.types';

function* createConferenceSaga({
  payload,
}: ReturnType<typeof conferenceActions.createConference>) {
  const conferenceName = uniqid().toLowerCase();
  yield put(
    wsSendMessage({
      type: WebsocketMessageTypes.CREATE_ROOM,
      data: { roomName: conferenceName, RTC: payload.RTC },
    }),
  );
}

function* joinConferenceSaga({
  payload,
}: ReturnType<typeof conferenceActions.joinConference>) {
  const RTC: ReturnType<typeof selectRTC> = yield select(selectRTC);
  if (RTC.name === 'jitsi') {
    yield put(jitsiActions.joinConference(payload));
  }
  if (RTC.name === 'zoom') {
    const JID = uniqid();
    yield put(zoomActions.connect(payload, JID));
    yield put(baseActions.setMyJid(JID));
  }
}

function* roomCreatedSaga({ data }: any) {
  const RTC: ReturnType<typeof selectRTC> = yield select(selectRTC);
  if (RTC.name === 'jitsi') {
    yield put(jitsiActions.createConference(data.roomName));
  }
  if (RTC.name === 'zoom') {
    const JID = uniqid();
    yield put(zoomActions.connect(data.roomName, JID));
    yield put(baseActions.setMyJid(JID));
  }
}

function* isSuccessfullJoinedSaga({
  payload,
}: ReturnType<typeof conferenceActions.isSuccessfullJoined>) {
  const UUID = yield select(selectUUID);
  const name = yield select(selectName);
  yield put(
    wsSendMessage({
      type: WebsocketMessageTypes.ROOM_USER_JOIN,
      data: {
        roomName: payload.conferenceName,
        player: { JID: payload.JID, UUID, name },
      },
    }),
  );
}

function* roomFullDataSaga({ data }: any) {
  yield put(conferenceActions.roomFullData(data));
}

function* playerConnectedSaga({ data }: any) {
  const newPlayer = { ...data.player, video: true, audio: true };
  yield put(conferenceActions.playerAdded(newPlayer));
}

function* playerDisconnectedSaga({ data }: any) {
  yield put(conferenceActions.playerRemoved(data.UUID));
}

function* toggleMuteMyselfSaga({
  payload,
}: ReturnType<typeof conferenceActions.toggleMuteMyself>) {
  const myJid: string = yield select(selectMyJid);
  const RTC: ReturnType<typeof selectRTC> = yield select(selectRTC);
  const isUnmuted: boolean = yield select(
    selectPlayerMuted(myJid, payload.trackType),
  );
  const roomName = yield select(selectConferenceName);

  if (RTC.name === 'jitsi') {
    if (isUnmuted) {
      yield put(jitsiActions.muteTrack(payload.trackType));
    } else {
      yield put(jitsiActions.unmuteTrack(payload.trackType));
    }
  } else {
    if (isUnmuted) {
      yield put(zoomActions.muteTrack(payload.trackType));
    } else {
      yield put(zoomActions.unmuteTrack(payload.trackType));
    }
  }
}

function* audioChangedSaga({ data }: any) {
  yield put(conferenceActions.changeAudio(data.status, data.JID));
}

function* videoChangedSaga({ data }: any) {
  yield put(conferenceActions.changeVideo(data.status, data.JID));
}

function* getRoomFullDataSaga({
  payload,
}: ReturnType<typeof conferenceActions.getRoomFullData>) {
  yield put(
    wsSendMessage({
      type: WebsocketMessageTypes.GET_ROOM_DATA,
      data: { roomName: payload },
    }),
  );
}

function* isConferenceLoadedSaga({
  payload,
}: ReturnType<typeof zoomActions.conferenceIsLoaded>) {
  yield put(conferenceActions.setIsLoaded(payload));
}

export default function* conferenceFlow() {
  yield takeEvery(ConferenceActions.CREATE_CONFERENCE, createConferenceSaga);
  yield takeEvery(WebsocketMessageTypes.ROOM_CREATED, roomCreatedSaga);
  yield takeEvery(
    ConferenceActions.IS_SUCCESSFULL_JOINED,
    isSuccessfullJoinedSaga,
  );
  yield takeEvery(WebsocketMessageTypes.PLAYER_CONNECTED, playerConnectedSaga);
  yield takeEvery(WebsocketMessageTypes.ROOM_FULL_DATA, roomFullDataSaga);
  yield takeEvery(
    WebsocketMessageTypes.PLAYER_DISCONNECTED,
    playerDisconnectedSaga,
  );
  yield takeEvery(ConferenceActions.TOGGLE_MUTE_MYSELF, toggleMuteMyselfSaga);
  yield takeEvery(WebsocketMessageTypes.AUDIO_CHANGED, audioChangedSaga);
  yield takeEvery(WebsocketMessageTypes.VIDEO_CHANGED, videoChangedSaga);
  yield takeEvery(ConferenceActions.GET_ROOM_FULL_DATA, getRoomFullDataSaga);
  yield takeEvery(ConferenceActions.JOIN_CONFERENCE, joinConferenceSaga);
  yield takeEvery(ZoomActionTypes.CONFERENCE_IS_LOADED, isConferenceLoadedSaga);
}
