export interface ConferenceState {
  conferenceName: string;
  isLoaded: boolean;
  players: Player[];
  isJoined: boolean;
  RTC: IRTC;
}

export enum ConferenceActions {
  IS_SUCCESSFULL_JOINED = 'IS_SUCCESSFULL_JOINED',
  PLAYER_ADDED = 'PLAYER_ADDED',
  CREATE_CONFERENCE = 'CREATE_CONFERENCE',
  SET_RTC = 'SET_RTC',
  ROOM_FULL_DATA = 'ROOM_FULL_DATA',
  PLAYER_REMOVED = 'PLAYER_REMOVED',
  TOGGLE_MUTE_MYSELF = 'TOGGLE_MUTE_MYSELF',
  CHANGE_AUDIO = 'CHANGE_AUDIO',
  CHANGE_VIDEO = 'CHANGE_VIDEO',
  GET_ROOM_FULL_DATA = 'GET_ROOM_FULL_DATA',
  JOIN_CONFERENCE = 'JOIN_CONFERENCE',
  SET_IS_LOADED = 'SET_IS_LOADED',
}

export interface Player {
  UUID: string;
  name: string;
  JID?: string;
  video?: boolean;
  audio?: boolean;
}

export interface IRTC {
  name: string;
  id: number;
}

export enum TrackTypes {
  audio = 'audio',
  video = 'video',
}

export type ConferenceHandler<T = undefined> = (
  state: ConferenceState,
  payload: T extends (...args: any[]) => infer R ? R : any,
) => ConferenceState | void;
