import {
  ConferenceActions,
  Player,
  IRTC,
  TrackTypes,
} from './conference.types';

export const isSuccessfullJoined = (
  isJoined: boolean,
  conferenceName: string,
  JID: string,
) => ({
  type: ConferenceActions.IS_SUCCESSFULL_JOINED,
  payload: { isJoined, conferenceName, JID },
});

export const playerAdded = (player: Player) => ({
  type: ConferenceActions.PLAYER_ADDED,
  payload: player,
});

export const createConference = (RTC: IRTC) => ({
  type: ConferenceActions.CREATE_CONFERENCE,
  payload: { RTC },
});

export const joinConference = (conferenceName: string) => ({
  type: ConferenceActions.JOIN_CONFERENCE,
  payload: conferenceName,
});

export const roomFullData = (data: any) => ({
  type: ConferenceActions.ROOM_FULL_DATA,
  payload: data,
});

export const setRTC = (RTC: IRTC) => ({
  type: ConferenceActions.SET_RTC,
  payload: RTC,
});

export const playerRemoved = (UUID) => ({
  type: ConferenceActions.PLAYER_REMOVED,
  payload: UUID,
});

export const toggleMuteMyself = (
  trackType: TrackTypes,
  localZoom?: boolean,
) => ({
  type: ConferenceActions.TOGGLE_MUTE_MYSELF,
  payload: {
    trackType,
    localZoom,
  },
});

export const changeAudio = (status: boolean, JID: string) => ({
  type: ConferenceActions.CHANGE_AUDIO,
  payload: { status, JID },
});

export const changeVideo = (status: boolean, JID: string) => ({
  type: ConferenceActions.CHANGE_VIDEO,
  payload: { status, JID },
});

export const getRoomFullData = (roomName: string) => ({
  type: ConferenceActions.GET_ROOM_FULL_DATA,
  payload: roomName,
});

export const setIsLoaded = (isLoaded: boolean) => ({
  type: ConferenceActions.SET_IS_LOADED,
  payload: isLoaded,
});
