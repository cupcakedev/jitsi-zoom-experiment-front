import {
  ConferenceState,
  ConferenceActions,
  ConferenceHandler,
} from './conference.types';
import * as conferenceActions from './conference.actions';
import { createReducer } from '@reduxjs/toolkit';

const initialState: ConferenceState = {
  conferenceName: '',
  isLoaded: false,
  players: [],
  isJoined: false,
  RTC: { name: 'jitsi', id: 1 },
};

const isSuccessfullJoinedHandler: ConferenceHandler<
  typeof conferenceActions.isSuccessfullJoined
> = (state: ConferenceState, { payload }) => ({
  ...state,
  isJoined: payload.isJoined,
  conferenceName: payload.conferenceName,
});

const playerAddedHandler: ConferenceHandler<
  typeof conferenceActions.playerAdded
> = (state: ConferenceState, { payload }) => ({
  ...state,
  players: [...state.players, payload],
});

const setRTCHandler: ConferenceHandler<typeof conferenceActions.setRTC> = (
  state,
  { payload },
) => ({
  ...state,
  RTC: payload,
});

const roomFullDataHandler: ConferenceHandler<
  typeof conferenceActions.roomFullData
> = (state, { payload }) => ({
  ...state,
  conferenceName: payload.name,
  players: payload.users,
  RTC: payload.RTC,
});

const playerRemovedHandler: ConferenceHandler<
  typeof conferenceActions.playerRemoved
> = (state, { payload }) => ({
  ...state,
  players: state.players.filter((player) => player.UUID !== payload),
});

const changeAudioHandler: ConferenceHandler<
  typeof conferenceActions.changeAudio
> = (state, { payload }) => ({
  ...state,
  players: state.players.map((player) => {
    if (player.JID === payload.JID) {
      return { ...player, audio: payload.status };
    }
    return player;
  }),
});

const changeVideoHandler: ConferenceHandler<
  typeof conferenceActions.changeVideo
> = (state, { payload }) => ({
  ...state,
  players: state.players.map((player) => {
    if (player.JID === payload.JID) {
      return { ...player, video: payload.status };
    }
    return player;
  }),
});

const setIsLoadedHandler: ConferenceHandler<
  typeof conferenceActions.setIsLoaded
> = (state, { payload }) => ({
  ...state,
  isLoaded: payload,
});

const handlers = {
  [ConferenceActions.IS_SUCCESSFULL_JOINED]: isSuccessfullJoinedHandler,
  [ConferenceActions.PLAYER_ADDED]: playerAddedHandler,
  [ConferenceActions.SET_RTC]: setRTCHandler,
  [ConferenceActions.ROOM_FULL_DATA]: roomFullDataHandler,
  [ConferenceActions.PLAYER_REMOVED]: playerRemovedHandler,
  [ConferenceActions.CHANGE_AUDIO]: changeAudioHandler,
  [ConferenceActions.CHANGE_VIDEO]: changeVideoHandler,
  [ConferenceActions.SET_IS_LOADED]: setIsLoadedHandler,
};

export default createReducer(initialState, handlers);
