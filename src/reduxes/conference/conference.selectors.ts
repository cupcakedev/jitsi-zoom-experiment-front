import { Participant } from '@zoom/videosdk';
import { ZoomState } from 'reduxes/zoom/zoom.types';
import { AppState } from 'types';
import { ConferenceState, Player, TrackTypes } from './conference.types';

export const selectIsJoined = (state: AppState): ConferenceState['isJoined'] =>
  state.conference.isJoined;

export const selectPlayers = (state: AppState): ConferenceState['players'] =>
  state.conference.players;

export const selectPlayerByJID = (JID: string) => (state: AppState) =>
  state.conference.players.find((player) => player.JID === JID);

export const selectRTC = (state: AppState): ConferenceState['RTC'] =>
  state.conference.RTC;

export const selectMyJid = (state: AppState): Player['JID'] => {
  const RTC = state.conference.RTC;
  if (RTC.name === 'jitsi') {
    return state.jitsi.myJid;
  } else {
    return state.base.myJID;
  }
};

export const selectConferenceName = (
  state: AppState,
): ConferenceState['conferenceName'] => state.conference.conferenceName;

export const selectPlayerMuted =
  (JID: string, trackType: TrackTypes) =>
  (state: AppState): Player['audio'] | Player['video'] => {
    const player = selectPlayerByJID(JID)(state);
    if (trackType === TrackTypes.audio) {
      return player.audio;
    } else {
      return player.video;
    }
  };

export const selectPlayersWithAudioTracks = (state: AppState): any => {
  const RTC = state.conference.RTC;
  const players = selectPlayers(state);
  if (RTC.name === 'jitsi') {
    const participants = state.jitsi.participants;
    return players.map((player) => ({
      ...player,
      audioTrack: participants[player.JID]?.audio,
    }));
  }
};

export const selectVideoTrack = (JID: string) => (state: AppState) => {
  const RTC = state.conference.RTC;
  if (RTC.name === 'jitsi') {
    if (state.jitsi.participants[JID]) {
      return state.jitsi.participants[JID]?.video;
    }
  }
};

export const selectAudioTrack = (JID: string) => (state: AppState) => {
  const RTC = state.conference.RTC;
  if (RTC.name === 'jitsi') {
    if (state.jitsi.participants[JID]) {
      return state.jitsi.participants[JID]?.audio;
    }
  }
};

export const selectPlayersWitshUserId = (state: AppState) => {
  const RTC = state.conference.RTC;
  const players = selectPlayers(state);
  if (RTC.name === 'zoom') {
    const participants = state.zoom.participants;
    return players.map((player) => {
      const participant = participants.find(
        (item) => item.displayName === player.JID,
      );
      return { ...player, userId: participant?.userId };
    });
  }
};

export const selectPlayersWithCameraAndAudioZoomState = (state: AppState) => {
  const RTC = state.conference.RTC;
  const players = selectPlayers(state);
  if (RTC.name === 'zoom') {
    const participants = state.zoom.participants;
    return players.map((player) => {
      const participant = participants.find(
        (item) => item.displayName === player.JID,
      );
      return {
        ...player,
        bVideoOn: participant?.bVideoOn,
        audioStatus: participant?.muted,
      };
    });
  }
};

export const selectPlayerWithZoomUserId =
  (JID: string) =>
  (state: AppState): any => {
    const playersWithUserId = selectPlayersWitshUserId(state);
    return playersWithUserId?.find((player) => player.JID === JID);
  };

export const selectZoomUserId =
  (JID: string) =>
  (state: AppState): Participant['userId'] =>
    state.zoom.participants.find((item) => item.displayName === JID)?.userId;

export const selectZoomMediaStream = (state: AppState): ZoomState['stream'] =>
  state.zoom.stream;

export const selectIsLoaded = (state: AppState): ConferenceState['isLoaded'] =>
  state.conference.isLoaded;

export const selectZoomParticipants = (
  state: AppState,
): ZoomState['participants'] => state.zoom.participants;

export const selectZoomParticipantsWithCameraOn = (
  state: AppState,
): Participant[] => state.zoom.participants.filter((item) => item.bVideoOn);
