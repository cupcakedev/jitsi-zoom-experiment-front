import { combineReducers } from 'redux';
import baseReducer from './base/base.reducer';
import conferenceReducer from './conference/conference.reducer';
import jitsiReducer from './jitsi/jitsi.reducer';
import zoomReducer from './zoom/zoom.reducer';

const rootReducer = combineReducers({
  base: baseReducer,
  jitsi: jitsiReducer,
  conference: conferenceReducer,
  zoom: zoomReducer,
});

export default rootReducer;
