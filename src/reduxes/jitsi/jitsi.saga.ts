import {
  actionChannel,
  call,
  fork,
  put,
  race,
  select,
  take,
} from 'redux-saga/effects';
import { buffers, eventChannel } from 'redux-saga';
import { JitsiService } from './JitsiService';
import { JitsiActionTypes, JitsiActionsAll } from './jitsi.types';
import * as jitsiActions from './jitsi.actions';
import * as conferenceActions from '../conference/conference.actions';
import { selectCameraDeviceId, selectMicDeviceId } from '../base/base.selector';
import {
  selectConferenceName,
  selectMyJid,
  selectParticipant,
} from './jitsi.selectors';
import { wsSendMessage } from 'reduxes/websockets/websocket.actions';
import { WebsocketMessageTypes } from 'reduxes/websockets/websockets.type';

const initJitsiChannel = (jitsiService: JitsiService) => {
  return eventChannel((emitter) => {
    jitsiService.setActionEmitter(emitter);

    return () => {
      console.log('disconnect');
    };
  }, buffers.fixed(100));
};

function* handleJitsiAction(jitsiService: JitsiService) {
  const jitsiCommandChan = yield actionChannel([
    JitsiActionTypes.JOIN_CONFERENCE,
    JitsiActionTypes.INITIALIZE,
    JitsiActionTypes.CREATE_LOCAL_TRACKS,
    JitsiActionTypes.CREATE_CONFERENCE,
    JitsiActionTypes.CONNECTION_ESTABLISHED,
    JitsiActionTypes.USER_ROLE_CHANGED,
    JitsiActionTypes.AUDIO_VIDEO_SETTINGS_CHANGED,
    JitsiActionTypes.MUTE_TRACK,
    JitsiActionTypes.UNMUTE_TRACK,
    JitsiActionTypes.TRACK_MUTE_CHANGED,
    JitsiActionTypes.CONFERENCE_FAILED,
    JitsiActionTypes.CONFERENCE_WRONG_PASSWORD,
  ]);

  while (true) {
    const action: JitsiActionsAll = yield take(jitsiCommandChan);

    switch (action.type) {
      case JitsiActionTypes.INITIALIZE:
        yield fork([jitsiService, 'initialize']);
        yield take(JitsiActionTypes.CONNECTION_ESTABLISHED);
        break;

      case JitsiActionTypes.CREATE_CONFERENCE:
        const {
          payload: { conferenceName },
        } = action as ReturnType<typeof jitsiActions.createConference>;
        yield call([jitsiService, 'createConference'], conferenceName);

        const joinedAction: ReturnType<typeof jitsiActions.conferenceJoined> =
          yield take(JitsiActionTypes.CONFERENCE_JOINED);

        // wait for jisi set moderator role then setup password
        console.log('create conference success');
        yield put(
          jitsiActions.createConferenceSuccess(
            conferenceName,
            joinedAction.payload.userJid,
          ),
        );
        console.log('is successful joined');
        yield put(
          conferenceActions.isSuccessfullJoined(
            true,
            conferenceName,
            joinedAction.payload.userJid,
          ),
        );
        try {
          const cameraDeviceId: ReturnType<typeof selectCameraDeviceId> =
            yield select(selectCameraDeviceId);
          const micDeviceId: ReturnType<typeof selectMicDeviceId> =
            yield select(selectMicDeviceId);
          yield call([jitsiService, 'createLocalTracks'], {
            cameraDeviceId,
            micDeviceId,
          });
        } catch (e) {}

        break;

      case JitsiActionTypes.JOIN_CONFERENCE: {
        console.log('try join');
        const {
          payload: { conferenceName },
        } = action as ReturnType<typeof jitsiActions.joinConference>;
        yield call([jitsiService, 'joinConference'], conferenceName);

        const {
          joinedAction,
        }: {
          joinedAction: ReturnType<typeof jitsiActions.conferenceJoined>;
        } = yield race({
          joinedAction: take(JitsiActionTypes.CONFERENCE_JOINED),
          failed: take(JitsiActionTypes.CONFERENCE_FAILED),
        });
        try {
          const cameraDeviceId: ReturnType<typeof selectCameraDeviceId> =
            yield select(selectCameraDeviceId);
          const micDeviceId: ReturnType<typeof selectMicDeviceId> =
            yield select(selectMicDeviceId);
          yield call([jitsiService, 'createLocalTracks'], {
            cameraDeviceId,
            micDeviceId,
          });
        } catch (e) {
          console.log(e);
        }

        yield put(
          jitsiActions.joinConferenceSuccess(
            conferenceName,
            joinedAction.payload.userJid,
          ),
        );

        yield put(
          conferenceActions.isSuccessfullJoined(
            true,
            conferenceName,
            joinedAction.payload.userJid,
          ),
        );
        break;
      }

      case JitsiActionTypes.AUDIO_VIDEO_SETTINGS_CHANGED: {
        try {
          const cameraDeviceId: ReturnType<typeof selectCameraDeviceId> =
            yield select(selectCameraDeviceId);
          const micDeviceId: ReturnType<typeof selectMicDeviceId> =
            yield select(selectMicDeviceId);
          yield call([jitsiService, 'recrateLoсalTracks'], {
            cameraDeviceId,
            micDeviceId,
          });
        } catch (e) {
          console.log(e);
        }
        break;
      }

      case JitsiActionTypes.MUTE_TRACK: {
        const _action = action as ReturnType<typeof jitsiActions.muteTrack>;
        const jid = yield select(selectMyJid);
        try {
          const participant = yield select(selectParticipant(jid));
          const track = participant[_action.payload.trackType];
          yield call([track, 'mute']);
        } catch (e) {
          yield put(
            jitsiActions.muteTrackFailure(jid, _action.payload.trackType),
          );
        }
        break;
      }

      case JitsiActionTypes.UNMUTE_TRACK: {
        const _action = action as ReturnType<typeof jitsiActions.unmuteTrack>;
        const jid = yield select(selectMyJid);
        try {
          const participant = yield select(selectParticipant(jid));
          const track = participant[_action.payload.trackType];
          yield call([track, 'unmute']);
        } catch (e) {
          yield put(
            jitsiActions.unmuteTrackFailure(jid, _action.payload.trackType),
          );
        }
        break;
      }

      case JitsiActionTypes.TRACK_MUTE_CHANGED: {
        const {
          payload: { trackType, track, jid },
        } = action as ReturnType<typeof jitsiActions.trackMuteChanged>;
        const isMuted = track.isMuted();
        const roomName = yield select(selectConferenceName);
        if (isMuted) {
          yield put(jitsiActions.muteTrackSuccess(jid, trackType));
          if (trackType === 'audio') {
            yield put(
              wsSendMessage({
                type: WebsocketMessageTypes.SET_AUDIO_MUTE,
                data: { roomName, JID: jid, status: false },
              }),
            );
          } else {
            yield put(
              wsSendMessage({
                type: WebsocketMessageTypes.SET_VIDEO_MUTE,
                data: { roomName, JID: jid, status: false },
              }),
            );
          }
        } else {
          yield put(jitsiActions.unmuteTrackSuccess(jid, trackType));
          if (trackType === 'audio') {
            yield put(
              wsSendMessage({
                type: WebsocketMessageTypes.SET_AUDIO_MUTE,
                data: { roomName, JID: jid, status: true },
              }),
            );
          } else {
            yield put(
              wsSendMessage({
                type: WebsocketMessageTypes.SET_VIDEO_MUTE,
                data: { roomName, JID: jid, status: true },
              }),
            );
          }
        }
        break;
      }
    }
  }
}

export default function* jitsiSagas() {
  const jitsiService: JitsiService = new JitsiService();

  yield fork(handleJitsiAction, jitsiService);

  const channel = yield call(initJitsiChannel, jitsiService);

  while (true) {
    const action = yield take(channel);
    yield put(action);
  }
}
