import * as jitsiActions from './jitsi.actions';

export enum JitsiActionTypes {
  CONNECTION_ESTABLISHED = '[JITSI->] Connection estabilished',
  CONNECTION_FAILED = '[JITSI->] Connection failed',
  CONFERENCE_JOINED = '[JITSI->] Conference joined',
  TRACK_ADDED = '[JITSI->] Track added',
  USER_ROLE_CHANGED = '[JITSI->] User role changed',
  CONFERENCE_FAILED = '[JITSI->] Conference failed',
  USER_JOINED = '[JITSI->] User joined',
  USER_LEFT = '[JITSI->] User left',
  TRACK_MUTE_CHANGED = '[JITSI->] Track mute changed',
  DISPLAY_NAME_CHANGED = '[JITSI->] Display name changed',
  CONNECTION_DISCONNECTED = '[JITSI->] Connection disconnected',
  TRACK_REMOVED = '[JITSI->] Track removed',
  CONFERENCE_ERROR = '[JITSI->] Conference error',
  CONFERENCE_WRONG_PASSWORD = '[JITSI->] Conference wrong password',

  MUTE_TRACK = '[JITSI<-]  Mute track',
  MUTE_TRACK_SUCCESS = '[JITSI->] Mute track success',
  MUTE_TRACK_FAILURE = '[JITSI->] Mute track failure',
  UNMUTE_TRACK = '[JITSI<-]  Unmute track',
  UNMUTE_TRACK_SUCCESS = '[JITSI->] Unmute track success',
  UNMUTE_TRACK_FAILURE = '[JITSI->] Unmute track failure',

  INITIALIZE = '[JITSI<-] Initialize jitsi',
  CREATE_LOCAL_TRACKS = '[JITSI<-] Create local tracks',
  CREATE_CONFERENCE = '[JITSI<-] Create conference',
  CREATE_CONFERENCE_SUCCESS = '[JITSI] Create conference success',
  JOIN_CONFERENCE = '[JITSI<-] Join conference',
  JOIN_CONFERENCE_SUCCESS = '[JITSI] Join conference success',
  AUDIO_VIDEO_SETTINGS_CHANGED = '[JITSI<-] Audio/video settings changed',
}

export interface Participant {
  jid: string;
  audio: any;
  video: any;
  isAudioMuted: boolean;
  isVideoMuted: boolean;
}

export interface JitsiState {
  conferenceName: string;
  isInitialized: boolean;
  isJoined: boolean;
  myJid: string;
  participants: {
    [key: string]: Participant;
  };
}

export type JitsiHandler<T = undefined> = (
  state: JitsiState,
  action: T extends (...args: any[]) => infer R ? R : any,
) => JitsiState | void;

export type JitsiOutgoingActionsAll =
  | ReturnType<typeof jitsiActions.connectionEstablished>
  | ReturnType<typeof jitsiActions.conferenceJoined>
  | ReturnType<typeof jitsiActions.connectionFailed>
  | ReturnType<typeof jitsiActions.trackAdded>
  | ReturnType<typeof jitsiActions.userRoleChanged>
  | ReturnType<typeof jitsiActions.conferenceFailed>
  | ReturnType<typeof jitsiActions.userJoined>
  | ReturnType<typeof jitsiActions.userLeft>
  | ReturnType<typeof jitsiActions.trackMuteChanged>
  | ReturnType<typeof jitsiActions.displayNameChanged>
  | ReturnType<typeof jitsiActions.connectionDisconnected>
  | ReturnType<typeof jitsiActions.conferenceError>;

export type JitsiActionsAll =
  | JitsiOutgoingActionsAll
  | ReturnType<typeof jitsiActions.initialize>
  | ReturnType<typeof jitsiActions.createLocalTracks>
  | ReturnType<typeof jitsiActions.createConference>
  | ReturnType<typeof jitsiActions.joinConference>
  | ReturnType<typeof jitsiActions.createConferenceSuccess>
  | ReturnType<typeof jitsiActions.joinConferenceSuccess>;

export type Devices = { cameraDeviceId?: string; micDeviceId?: string };
