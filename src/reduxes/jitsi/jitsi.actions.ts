import { JitsiActionTypes } from './jitsi.types';

export const initialize = () => ({
  type: JitsiActionTypes.INITIALIZE,
});

export const connectionEstablished = () => ({
  type: JitsiActionTypes.CONNECTION_ESTABLISHED,
});

export const conferenceJoined = (bareJid: string, userJid: string) => ({
  type: JitsiActionTypes.CONFERENCE_JOINED,
  payload: {
    bareJid,
    userJid,
  },
});

export const connectionFailed = () => ({
  type: JitsiActionTypes.CONNECTION_FAILED,
});

export const trackAdded = (
  jid: string,
  type: 'audio' | 'video',
  track: any,
  isMuted: boolean,
) => ({
  type: JitsiActionTypes.TRACK_ADDED,
  payload: {
    jid,
    type,
    track,
    isMuted,
  },
});

export const userRoleChanged = (jid: string, role: string) => ({
  type: JitsiActionTypes.USER_ROLE_CHANGED,
  payload: { jid, role },
});

export const conferenceFailed = () => ({
  type: JitsiActionTypes.CONFERENCE_FAILED,
});

export const userJoined = () => ({
  type: JitsiActionTypes.USER_JOINED,
});

export const userLeft = (jid) => ({
  type: JitsiActionTypes.USER_LEFT,
  payload: jid,
});

export const conferenceWrongPassword = () => ({
  type: JitsiActionTypes.CONFERENCE_WRONG_PASSWORD,
});

export const trackMuteChanged = (
  jid: string,
  trackType: 'audio' | 'video',
  track: any,
) => ({
  type: JitsiActionTypes.TRACK_MUTE_CHANGED,
  payload: {
    jid,
    trackType,
    track,
  },
});

export const displayNameChanged = () => ({
  type: JitsiActionTypes.DISPLAY_NAME_CHANGED,
});

export const connectionDisconnected = () => ({
  type: JitsiActionTypes.CONNECTION_DISCONNECTED,
});

export const conferenceError = () => ({
  type: JitsiActionTypes.CONFERENCE_ERROR,
});

export const trackRemoved = (jid: string, type: 'audio' | 'video', track) => ({
  type: JitsiActionTypes.TRACK_REMOVED,
  payload: {
    jid,
    type,
    track,
  },
});

export const createLocalTracks = () => ({
  type: JitsiActionTypes.CREATE_LOCAL_TRACKS,
});

export const muteTrack = (trackType: 'audio' | 'video') => ({
  type: JitsiActionTypes.MUTE_TRACK,
  payload: {
    trackType,
  },
});
export const muteTrackSuccess = (
  jid: string,
  trackType: 'audio' | 'video',
) => ({
  type: JitsiActionTypes.MUTE_TRACK_SUCCESS,
  payload: {
    trackType,
    jid,
  },
});
export const muteTrackFailure = (
  jid: string,
  trackType: 'audio' | 'video',
) => ({
  type: JitsiActionTypes.MUTE_TRACK_FAILURE,
  payload: {
    trackType,
    jid,
  },
});

export const unmuteTrack = (trackType: 'audio' | 'video') => ({
  type: JitsiActionTypes.UNMUTE_TRACK,
  payload: {
    trackType,
  },
});
export const unmuteTrackSuccess = (
  jid: string,
  trackType: 'audio' | 'video',
) => ({
  type: JitsiActionTypes.UNMUTE_TRACK_SUCCESS,
  payload: {
    trackType,
    jid,
  },
});
export const unmuteTrackFailure = (
  jid: string,
  trackType: 'audio' | 'video',
) => ({
  type: JitsiActionTypes.UNMUTE_TRACK_FAILURE,
  payload: {
    trackType,
    jid,
  },
});

export const createConference = (conferenceName) => ({
  type: JitsiActionTypes.CREATE_CONFERENCE,
  payload: { conferenceName },
});

export const createConferenceSuccess = (
  conferenceName: string,
  myJid: string,
) => ({
  type: JitsiActionTypes.CREATE_CONFERENCE_SUCCESS,
  payload: {
    conferenceName,
    myJid,
  },
});
export const joinConference = (conferenceName: string) => ({
  type: JitsiActionTypes.JOIN_CONFERENCE,
  payload: {
    conferenceName,
  },
});

export const joinConferenceSuccess = (
  conferenceName: string,
  myJid: string,
) => ({
  type: JitsiActionTypes.JOIN_CONFERENCE_SUCCESS,
  payload: {
    conferenceName,
    myJid,
  },
});
