import { MediaTrackConstraints } from 'types';
import { JitsiOutgoingActionsAll, Devices } from './jitsi.types';
import * as jitsiActions from './jitsi.actions';

export const conferenceConfig = {
  hosts: {
    domain: 'warp.liars.club',
    muc: 'conference.warp.liars.club',
    bridge: 'jitsi-videobridge.warp.liars.club',
  },
  bosh: '//warp.liars.club/http-bind',
  clientNode: 'http://jitsi.org/jitsimeet',
  disableThirdPartyRequests: true,
  enableAnalyticsLogging: false,
};

export const initConfig = {
  disableAudioLevels: true,

  // The ID of the jidesha extension for Chrome.
  desktopSharingChromeExtId: 'mbocklcggfhnbahlnepmldehdhpjfcjp',

  // Whether desktop sharing should be disabled on Chrome.
  desktopSharingChromeDisabled: false,

  // The media sources to use when using screen sharing with the Chrome
  // extension.
  desktopSharingChromeSources: ['screen', 'window'],

  // Required version of Chrome extension
  desktopSharingChromeMinExtVersion: '0.1',

  // Whether desktop sharing should be disabled on Firefox.
  desktopSharingFirefoxDisabled: true,

  // w3c spec-compliant video constraints to use for video capture. Currently
  // used by browsers that return true from lib-jitsi-meet's
  // util#browser#usesNewGumFlow. The constraints are independency from
  // this config's resolution value. Defaults to requesting an ideal aspect
  // ratio of 16:9 with an ideal resolution of 720.
  disableSimulcast: false,
  enableRemb: true,
  enableTcc: true,
  resolution: 720,
  constraints: {
    video: {
      height: {
        ideal: 720,
        max: 720,
        min: 180,
      },
      width: {
        ideal: 1280,
        max: 1280,
        min: 320,
      },
    },
  },
  enableP2P: false, // flag to control P2P connections
  // New P2P options
  p2p: {
    enabled: false,
    preferH264: true,
    disableH264: true,
    useStunTurn: true, // use XEP-0215 to fetch STUN and TURN servers for the P2P connection
  },
  startBitrate: '400',
};

export const audioConstraints: MediaTrackConstraints = {
  echoCancellation: true,
};

const roomOptions = {
  openBridgeChannel: true,
};

export class JitsiService {
  // constructor() {}

  private emitter?: (event: JitsiOutgoingActionsAll) => void;

  private emitAction(event: JitsiOutgoingActionsAll) {
    this.emitter && this.emitter(event);
  }

  public setActionEmitter(emitter: (event: JitsiOutgoingActionsAll) => void) {
    this.emitter = emitter;
  }

  connection: any;

  room: any;

  private conferenceName: string;
  private conferencePassword: string;

  public async initialize(): Promise<void> {
    // await  applyVideoConstraints(videoConstraints)
    JitsiMeetJS.init(initConfig);
    JitsiMeetJS.setLogLevel(JitsiMeetJS.logLevels.ERROR);
    this.connection = new JitsiMeetJS.JitsiConnection(
      null,
      null,
      conferenceConfig,
    );
    this.connection.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED,
      () => {
        console.log('jitsi connection estabilished');
        this.emitAction(jitsiActions.connectionEstablished());
      },
    );
    this.connection.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_FAILED,
      () => {
        console.log('jitsi connection failed');
        this.emitAction(jitsiActions.connectionFailed());
      },
    );

    JitsiMeetJS.mediaDevices.addEventListener(
      JitsiMeetJS.events.mediaDevices.DEVICE_LIST_CHANGED,
      () => {
        console.log('devices changed');
      },
    );

    this.connection.connect();
  }
  private handleRemoteTrackAdded(track: any) {
    console.log('track added', track.getType(), track);
    this.emitAction(
      jitsiActions.trackAdded(
        track.getParticipantId(),
        track.getType(),
        track,
        track.isMuted(),
      ),
    );
  }
  private handleTrackRemoved(track: any) {
    console.log('track removed', track);
    this.emitAction(
      jitsiActions.trackRemoved(
        track.getParticipantId(),
        track.getType(),
        track,
      ),
    );
  }
  private handleTrackMuteChanged(track: any) {
    console.log('track mute changed', track);
    this.emitAction(
      jitsiActions.trackMuteChanged(
        track.getParticipantId(),
        track.getType(),
        track,
      ),
    );
  }

  private handleDisplayNameChanged(userId: any, displayName: any) {
    console.log('display name changed', userId, displayName);
    this.emitAction(jitsiActions.displayNameChanged());
  }

  private handleConnectionDisconnected() {
    console.log('connection disconnected');
    this.emitAction(jitsiActions.connectionDisconnected());
  }

  private handleConferenceJoined() {
    console.log('conference joined');
    const bareJid = this.room.connection.xmpp.connection.jid;
    const userJid = this.room.myUserId();
    this.emitAction(jitsiActions.conferenceJoined(bareJid, userJid));
  }

  private handleUserRoleChanged(id: string, role: string) {
    console.log('user role changed', id, role);
    this.emitAction(jitsiActions.userRoleChanged(id, role));
    if (this.getMyUserJid() === id) {
      // my role changed
    }
  }

  private handleConferenceFailed(err: any) {
    console.log('conference failed', err);
    this.emitAction(jitsiActions.conferenceFailed());
    //  this.closeRoom();
  }

  private handleUserJoined(id: string, user: any) {
    console.log('user joined', id, user);
    this.emitAction(jitsiActions.userJoined());
  }

  private handleUserLeft(jid: string) {
    console.log('user left', jid);
    this.emitAction(jitsiActions.userLeft(jid));
  }

  private handleConferenceError() {
    console.log('conference error');
    this.emitAction(jitsiActions.conferenceError());
  }

  addConferenceListeners(): void {
    this.room.on(
      JitsiMeetJS.events.conference.TRACK_ADDED,
      this.handleRemoteTrackAdded.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.CONFERENCE_JOINED,
      this.handleConferenceJoined.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.USER_ROLE_CHANGED,
      this.handleUserRoleChanged.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.CONFERENCE_FAILED,
      this.handleConferenceFailed.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.USER_JOINED,
      this.handleUserJoined.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.USER_LEFT,
      this.handleUserLeft.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.TRACK_MUTE_CHANGED,
      this.handleTrackMuteChanged.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.DISPLAY_NAME_CHANGED,
      this.handleDisplayNameChanged.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,
      this.handleConnectionDisconnected.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.CONFERENCE_ERROR,
      this.handleConferenceError.bind(this),
    );
    this.room.on(
      JitsiMeetJS.events.conference.TRACK_REMOVED,
      this.handleTrackRemoved.bind(this),
    );
  }

  removeConferenceListeners(): void {
    this.room.off(
      JitsiMeetJS.events.conference.TRACK_ADDED,
      this.handleRemoteTrackAdded.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.CONFERENCE_JOINED,
      this.handleConferenceJoined.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.USER_ROLE_CHANGED,
      this.handleUserRoleChanged.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.CONFERENCE_FAILED,
      this.handleConferenceFailed.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.USER_JOINED,
      this.handleUserJoined.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.USER_LEFT,
      this.handleUserLeft.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.TRACK_MUTE_CHANGED,
      this.handleTrackMuteChanged.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.DISPLAY_NAME_CHANGED,
      this.handleDisplayNameChanged.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,
      this.handleConnectionDisconnected.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.CONFERENCE_ERROR,
      this.handleConferenceError.bind(this),
    );
    this.room.off(
      JitsiMeetJS.events.conference.TRACK_REMOVED,
      this.handleTrackRemoved.bind(this),
    );
  }

  private getMyUserJid(): string {
    return this.room.myUserId();
  }

  public async createLocalTracks({ cameraDeviceId, micDeviceId }: Devices) {
    try {
      const [audioTrack] = this.room.getLocalTracks();
      await Promise.all([this.room.removeTrack(audioTrack)]);
    } catch (e) {
      console.log(e);
    }

    try {
      const [videoTrack] = this.room.getLocalTracks();
      await Promise.all([this.room.removeTrack(videoTrack)]);
    } catch (e) {
      console.log(e);
    }

    try {
      const [audioTrack] = await JitsiMeetJS.createLocalTracks({
        devices: ['audio'],
        micDeviceId: micDeviceId,
      });
      this.room.addTrack(audioTrack);
    } catch (e) {
      console.error(e);
    }

    try {
      const [videoTrack] = await JitsiMeetJS.createLocalTracks({
        devices: ['video'],
        cameraDeviceId: cameraDeviceId,
      });
      this.room.addTrack(videoTrack);
    } catch (e) {
      console.error(e);
    }
  }

  public async recrateLoсalTracks({
    cameraDeviceId,
    micDeviceId,
  }: Devices): Promise<void> {
    if (this.room) {
      // first, try audio
      try {
        const [audioTrack] = this.room.getLocalTracks();
        const [newAudioTrack] = await JitsiMeetJS.createLocalTracks({
          devices: ['audio'],
          micDeviceId: micDeviceId,
        });

        await Promise.all([this.room.removeTrack(audioTrack)]);

        this.room.addTrack(newAudioTrack);
      } catch (e) {
        console.error(e);
      }
      // second, try video
      try {
        const [videoTrack] = this.room.getLocalTracks();
        const [newVideoTrack] = await JitsiMeetJS.createLocalTracks({
          devices: ['video'],
          cameraDeviceId: cameraDeviceId,
        });

        await Promise.all([this.room.removeTrack(videoTrack)]);

        this.room.addTrack(newVideoTrack);
      } catch (e) {
        console.error(e);
      }
    }
  }

  public createConference(conferenceName: string) {
    this.room = this.connection.initJitsiConference(
      conferenceName,
      roomOptions,
    );
    this.conferenceName = conferenceName;
    this.addConferenceListeners();
    this.room.join();
  }

  public async joinConference(conferenceName: string) {
    if (!this.room) {
      try {
        this.room = this.connection.initJitsiConference(
          conferenceName,
          roomOptions,
        );
        console.log('room', this.room);
        this.addConferenceListeners();
      } catch (e) {
        console.log('init confererence fail');
      }
    }
    this.conferenceName = conferenceName;

    this.room.join();
  }

  public async closeRoom() {
    try {
      const [audioTrack, videoTrack] = this.room.getLocalTracks();
      await Promise.all([
        this.room.removeTrack(audioTrack),
        this.room.removeTrack(videoTrack),
      ]);
      if (audioTrack) {
        audioTrack.dispose();
      }
      if (videoTrack) {
        videoTrack.dispose();
      }
    } catch (e) {
      console.log(e);
    }
    this.room.leave();
    // this.connection.disconnect();
    this.removeConferenceListeners();
    this.room = null;
  }
}
