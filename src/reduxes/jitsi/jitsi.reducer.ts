import { createReducer } from '@reduxjs/toolkit';
import { JitsiState, JitsiHandler, JitsiActionTypes } from './jitsi.types';
import * as jitsiActions from './jitsi.actions';

const initialState: JitsiState = {
  conferenceName: '',
  isInitialized: false,
  myJid: '',
  isJoined: false,
  participants: {},
};

const connectionEstablishedHandler: JitsiHandler<
  typeof jitsiActions.connectionEstablished
> = (state) => ({
  ...state,
  isInitialized: true,
});

const trackAddedHandler: JitsiHandler<typeof jitsiActions.trackAdded> = (
  state,
  { payload: { jid, track, type, isMuted } },
) => ({
  ...state,
  participants: {
    ...state.participants,
    [jid]: {
      ...state.participants[jid],
      [type]: track,
      isAudioMuted:
        type === 'audio'
          ? isMuted
          : state.participants[jid] && state.participants[jid].isAudioMuted,
      isVideoMuted:
        type === 'video'
          ? isMuted
          : state.participants[jid] && state.participants[jid].isVideoMuted,
    },
  },
});

const trackRemovedHandler: JitsiHandler<typeof jitsiActions.trackRemoved> = (
  state,
  { payload: { jid, type } },
) => {
  return {
    ...state,
    participants: {
      ...state.participants,
      [jid]: {
        ...state.participants[jid],
        [type]: null,
      },
    },
  };
};

const muteTrackSuccessHandler: JitsiHandler<
  typeof jitsiActions.muteTrackSuccess
> = (state, { payload: { jid, trackType } }) => {
  return {
    ...state,
    participants: {
      ...state.participants,
      [jid]: {
        ...state.participants[jid],
        [extractTrackStatusFieldName(trackType)]: true,
      },
    },
  };
};

const unmuteTrackSuccessHandler: JitsiHandler<
  typeof jitsiActions.unmuteTrackSuccess
> = (state, { payload: { jid, trackType } }) => {
  return {
    ...state,
    participants: {
      ...state.participants,
      [jid]: {
        ...state.participants[jid],
        [extractTrackStatusFieldName(trackType)]: false,
      },
    },
  };
};

const extractTrackStatusFieldName = (trackType: 'audio' | 'video') => {
  switch (trackType) {
    case 'video':
      return 'isVideoMuted';
    case 'audio':
      return 'isAudioMuted';
  }
};

const joinConferenceHandler: JitsiHandler<typeof jitsiActions.joinConference> =
  (state) => ({
    ...state,
    isLastPasswordWrong: false,
  });

const joinConferenceSuccessHandler: JitsiHandler<
  typeof jitsiActions.joinConferenceSuccess
> = (state, { payload: { conferenceName, myJid } }) => ({
  ...state,
  conferenceName,
  myJid,
  isJoined: true,
});

const createConferenceSuccessHandler: JitsiHandler<
  typeof jitsiActions.createConferenceSuccess
> = (state, { payload: { conferenceName, myJid } }) => ({
  ...state,
  conferenceName,
  myJid,
  isJoined: true,
});

const handlers = {
  [JitsiActionTypes.CONNECTION_ESTABLISHED]: connectionEstablishedHandler,
  [JitsiActionTypes.TRACK_ADDED]: trackAddedHandler,
  [JitsiActionTypes.TRACK_REMOVED]: trackRemovedHandler,
  [JitsiActionTypes.MUTE_TRACK_SUCCESS]: muteTrackSuccessHandler,
  [JitsiActionTypes.UNMUTE_TRACK_SUCCESS]: unmuteTrackSuccessHandler,
  [JitsiActionTypes.JOIN_CONFERENCE]: joinConferenceHandler,
  [JitsiActionTypes.JOIN_CONFERENCE_SUCCESS]: joinConferenceSuccessHandler,
  [JitsiActionTypes.CREATE_CONFERENCE_SUCCESS]: createConferenceSuccessHandler,
};

export default createReducer(initialState, handlers);
