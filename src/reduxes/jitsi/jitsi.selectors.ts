import { AppState } from 'types';
import { JitsiState, Participant } from './jitsi.types';

export const selectParticipants = (
  state: AppState,
): JitsiState['participants'] => state.jitsi.participants;
export const selectParticipant =
  (jid: string) =>
  (state: AppState): Participant =>
    state.jitsi.participants[jid];

export const selectMyJid = (state: AppState): JitsiState['myJid'] =>
  state.jitsi.myJid;

export const selectVideoTrack = (JID: string) => (state: AppState) => {
  if (state.jitsi.participants[JID]) {
    return state.jitsi.participants[JID].video;
  }
};

export const selectAudioTrack = (JID: string) => (state: AppState) => {
  if (state.jitsi.participants[JID]) {
    return state.jitsi.participants[JID].audio;
  }
};

export const selectConferenceName = (
  state: AppState,
): JitsiState['conferenceName'] => state.jitsi.conferenceName;
