type Callback = (isSpeaking: boolean) => void;

class VoiceAnalyzer {
  isSpeaking = false;

  callback: Callback;

  audioTrack: any;

  audioContext: AudioContext;

  dataArray: Uint8Array;

  soundAnalyser: AnalyserNode;

  audioNode: MediaStreamAudioSourceNode | undefined;

  requestAnimationFrameId: number | undefined;

  voiceInsensitivity = 10;

  silenceValue = 128;

  minTrigger = this.silenceValue - this.voiceInsensitivity;

  maxTrigger = this.silenceValue + this.voiceInsensitivity;

  lastFalseChange = 0;

  silenceTimeout = 500;

  isWaitingForVoice = false;

  constructor(callback: Callback) {
    this.callback = callback;
    this.audioContext = new AudioContext();
    this.soundAnalyser = this.audioContext.createAnalyser();
    this.soundAnalyser.fftSize = 32;
    this.dataArray = new Uint8Array(this.soundAnalyser.frequencyBinCount);
  }

  update(audioTrack: MediaStream | undefined) {
    if (this.requestAnimationFrameId !== undefined) {
      cancelAnimationFrame(this.requestAnimationFrameId);
    }
    if (this.audioNode) {
      this.audioNode.disconnect();
    }
    this.audioTrack = audioTrack;
    if (!audioTrack) {
      return;
    }

    try {
      this.audioNode = this.audioContext.createMediaStreamSource(
        this.audioTrack,
      );
      this.audioNode.connect(this.soundAnalyser);
      this.runAnalyse();
    } catch (err) {
      // console.log('Error in voice analyzer:', err);
      // do nothing
    }
  }

  runAnalyse() {
    if (!this.audioTrack) {
      return;
    }
    this.soundAnalyser.getByteTimeDomainData(this.dataArray);

    const isSpeaking = this.dataArray.some(this.checkDataUnit.bind(this));

    if (isSpeaking === true) {
      this.isWaitingForVoice = false;
    }

    if (isSpeaking === false && this.isWaitingForVoice === false) {
      this.isWaitingForVoice = true;
      this.lastFalseChange = new Date().getTime();
    }

    if (isSpeaking !== this.isSpeaking) {
      if (
        this.isSilenceForLongTime.call(this, isSpeaking) ||
        isSpeaking === true
      ) {
        this.speakingChange.call(this, isSpeaking);
      }
    }

    requestAnimationFrame(this.runAnalyse.bind(this));
  }

  isSilenceForLongTime(isSpeaking: boolean) {
    return (
      isSpeaking === false &&
      this.isWaitingForVoice === true &&
      new Date().getTime() > this.lastFalseChange + this.silenceTimeout
    );
  }

  checkDataUnit(unit: number) {
    if (unit >= this.maxTrigger || unit <= this.minTrigger) {
      return true;
    }
    return false;
  }

  speakingChange(isSpeaking: boolean) {
    this.isSpeaking = isSpeaking;
    this.callback(isSpeaking);
  }
}

export default VoiceAnalyzer;
