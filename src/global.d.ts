declare const JitsiMeetJS: {
  createLocalTracks: (settings: {
    devices: ('audio' | 'video')[];
    cameraDeviceId?: string | null;
    micDeviceId?: string | null;
    resolution?: number;
    constraints?: any;
  }) => Promise;
  [key: string]: any;
};
