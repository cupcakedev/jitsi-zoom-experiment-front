import React from 'react';
import { Provider } from 'react-redux';
import { makeStore } from 'reduxes/store';
import { BrowserRouter, Route } from 'react-router-dom';
import { CreateConferencePage } from 'pages/CreateConferencePage';
import { ConferencePage } from 'pages/ConferencePage';

const App: React.FC = () => {
  const store = makeStore();
  return (
    <>
      <Provider store={store}>
        <BrowserRouter>
          <Route path="/" exact component={CreateConferencePage} />
          <Route path="/:id" exact component={ConferencePage} />
        </BrowserRouter>
      </Provider>
    </>
  );
};

export default App;
