import styled from 'styled-components';

const PRIMARY_COLOR = '#14aa53';

export const Button = styled.button`
  background: ${PRIMARY_COLOR};
  box-shadow: 0 4px 20px 0 rgba(0, 0, 0, 0.14), 0 7px 10px -5px ${PRIMARY_COLOR};
  color: white;
  padding: 12px 30px;
  border: none;
  text-transform: uppercase;
  border-radius: 3px;
  cursor: pointer;
  transition: all 0.3s ease;
  font-family: 'Optima';

  &:hover {
    background: white;
    color: #888888;
  }
`;

export const ActionSelectStyles = {
  container: (provided: any) => {
    return {
      ...provided,
      width: '100%',
    };
  },
  control: (provided: any) => {
    return {
      ...provided,
      border: '1px solid #000000',
      borderLeft: 'none',
      borderTop: 'none',
      borderRight: 'none',
      borderRadius: '0',
      color: '#000000',
      boxSizing: 'border-box',
      boxShadow: 'none',
      '&:hover': {
        borderColor: '1px solid #000000',
      },
    };
  },
  valueContainer: (provided: any) => {
    return {
      ...provided,
      width: '100%',
      padding: '0px',
    };
  },
  option: (provided: any, state: any) => {
    return {
      ...provided,
      width: '100%',
      backgroundColor: state.isSelected ? PRIMARY_COLOR : 'transparent',
    };
  },
};
