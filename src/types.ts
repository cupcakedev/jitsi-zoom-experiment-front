import { BaseState } from 'reduxes/base/base.types';
import { ConferenceState } from 'reduxes/conference/conference.types';
import { JitsiState } from 'reduxes/jitsi/jitsi.types';
import { ZoomState } from 'reduxes/zoom/zoom.types';

export type AppState = Readonly<{
  base: BaseState;
  jitsi: JitsiState;
  conference: ConferenceState;
  zoom: ZoomState;
}>;

interface MediaTrackConstraintValues {
  min: number;
  max: number;
  ideal?: number;
}

export interface MediaTrackConstraints {
  width?: MediaTrackConstraintValues;
  height?: MediaTrackConstraintValues;
  resizeMode?: 'crop-and-scale' | 'none';
  echoCancellation?: boolean;
}
