import React, { useCallback } from 'react';
import { Redirect } from 'react-router';
import * as SC from './styles';
import * as conferenceActions from 'reduxes/conference/conference.actions';
import Select from 'react-select';
import { useDispatch, useSelector } from 'react-redux';
import { IRTC } from 'reduxes/conference/conference.types';
import {
  selectIsJoined,
  selectRTC,
  selectConferenceName,
} from 'reduxes/conference/conference.selectors';
import { Button, ActionSelectStyles } from 'globalStyles';
import { selectName } from 'reduxes/base/base.selector';
import { setName } from 'reduxes/base/base.actions';

export const CreateConferencePage: React.FC = () => {
  const dispatch = useDispatch();
  const currentRTC = useSelector(selectRTC);
  const isJoined = useSelector(selectIsJoined);
  const conferenceName = useSelector(selectConferenceName);
  const name = useSelector(selectName);
  const RTCs: IRTC[] = [
    { name: 'jitsi', id: 1 },
    { name: 'zoom', id: 2 },
  ];
  const onChange = useCallback(
    (RTC: IRTC) => {
      dispatch(conferenceActions.setRTC(RTC));
    },
    [dispatch],
  );
  const createHandle = () => {
    dispatch(conferenceActions.createConference(currentRTC));
  };

  console.log(currentRTC);

  if (isJoined) {
    return <Redirect to={`/${conferenceName}`} />;
  }

  return (
    <SC.Root>
      <SC.CreateConferenceMenu>
        <div>Select RTC provider</div>
        <Select
          styles={ActionSelectStyles}
          value={currentRTC}
          getOptionLabel={(option) => option.name}
          getOptionValue={(option) => option.name}
          onChange={onChange}
          options={RTCs}
        />
        <div>Input your name</div>
        <input
          style={{ width: '90%', fontSize: '22px' }}
          type="text"
          value={name}
          onChange={(e) => dispatch(setName(e.target.value))}
        />
        <Button onClick={createHandle}>CREATE CONFERENCE</Button>
      </SC.CreateConferenceMenu>
    </SC.Root>
  );
};
