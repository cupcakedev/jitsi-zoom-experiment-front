import { PlayersAudioOutput } from 'components/PlayersAudioOutputs';
import { VideoBox } from 'components/VideoBox/VideoBox';
import { PlayerContext } from 'contexts/PlayerContext';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import {
  selectIsJoined,
  selectIsLoaded,
  selectPlayers,
  selectRTC,
  selectPlayersWithCameraAndAudioZoomState,
} from 'reduxes/conference/conference.selectors';
import * as conferenceActions from 'reduxes/conference/conference.actions';
import * as SC from './styles';
import { selectName, selectUUID } from 'reduxes/base/base.selector';
import { Button } from 'globalStyles';
import { setName } from 'reduxes/base/base.actions';
import { leave } from 'reduxes/zoom/zoom.actions';

export const ConferencePage: React.FC = () => {
  const dispatch = useDispatch();
  const isJoined = useSelector(selectIsJoined);
  const isLoaded = useSelector(selectIsLoaded);
  const RTC = useSelector(selectRTC);
  const players = useSelector(selectPlayers);
  const myUUID = useSelector(selectUUID);
  const name = useSelector(selectName);
  const { id } = useParams<{ id?: string }>();
  const zoomPlayers = useSelector(selectPlayersWithCameraAndAudioZoomState);

  useEffect(() => {
    if (!isJoined && myUUID) {
      dispatch(conferenceActions.getRoomFullData(id));
    }
  }, [isJoined, myUUID, dispatch, id]);

  useEffect(() => {
    if (RTC.name === 'zoom') {
      window.addEventListener('beforeunload', beforeUnloadHandler);
    }
  });

  const beforeUnloadHandler = () => {
    dispatch(leave());
  };

  const boxWidthAndHeight = {
    height: 350,
    width: 500,
  };

  const handleJoin = () => {
    dispatch(conferenceActions.joinConference(id));
  };

  if (!isJoined && !isLoaded) {
    return (
      <SC.Root>
        <SC.CreateConferenceMenu>
          <div>Input your name</div>
          <input
            style={{ width: '90%', fontSize: '22px' }}
            type="text"
            value={name}
            onChange={(e) => dispatch(setName(e.target.value))}
          />
          <Button onClick={handleJoin}>JOIN</Button>
        </SC.CreateConferenceMenu>
      </SC.Root>
    );
  }

  return (
    <SC.Root>
      <SC.PlayersWrapper>
        {RTC.name === 'jitsi'
          ? players.map((player) => (
              <PlayerContext.Provider
                key={player.UUID}
                value={{
                  JID: player.JID,
                  UUID: player.UUID,
                  name: player.name,
                  video: player.video,
                  audio: player.audio,
                }}
              >
                <VideoBox boxWidthAndHeight={boxWidthAndHeight} />
              </PlayerContext.Provider>
            ))
          : zoomPlayers.map((player) => (
              <PlayerContext.Provider
                key={player.JID}
                value={{
                  JID: player.JID,
                  UUID: player.UUID,
                  name: player.name,
                  video: player.bVideoOn,
                  audio: !player.audioStatus,
                }}
              >
                <VideoBox boxWidthAndHeight={boxWidthAndHeight} />
              </PlayerContext.Provider>
            ))}
      </SC.PlayersWrapper>
      {isJoined && RTC.name === 'jitsi' && <PlayersAudioOutput />}
    </SC.Root>
  );
};
