import styled from 'styled-components';

export const Root = styled.div`
  width: 100%;
  height: 100vh;
  padding: 30px;
  background-color: gray;
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
`;

export const CreateConferenceMenu = styled.div`
  width: 600px;
  height: 600px;
  padding: 10px;
  margin: 0 auto;
  background-color: white;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;

export const PlayersWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: space-around;
  flex-wrap: wrap;
`;
