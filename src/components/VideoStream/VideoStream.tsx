import { PlayerContext } from 'contexts/PlayerContext';
import React, { FC, useEffect, useRef, useContext } from 'react';
import * as SC from './styles';

interface Props {
  videoTrack: any;
}

const VideoStream: FC<Props> = ({ videoTrack }) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const { video } = useContext(PlayerContext);

  useEffect(() => {
    const video = videoRef.current;

    if (videoTrack && video) {
      if (typeof videoTrack.unmute !== 'function') {
        console.log('attach', videoTrack.track.id);
      }
      console.log('attach', videoTrack.track.id);

      videoTrack.attach(video);
    }
    return () => {
      if (videoTrack && video) {
        if (typeof videoTrack.unmute !== 'function') {
          console.log('detach', videoTrack.track.id);
        }
        console.log('detach', videoTrack.track.id);
        videoTrack.detach(video);
      }
    };
  }, [videoTrack, videoRef, video]);

  return (
    <SC.Root data-testid="videostream_root">
      {video && <SC.VideoRoot autoPlay={true} ref={videoRef} />}
    </SC.Root>
  );
};

export default VideoStream;
