import styled from 'styled-components';

export const Root = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const VideoRoot = styled.video`
  width: 100%;
  height: 100%;
  object-fit: cover;
  transform: rotateY(180deg);
`;
