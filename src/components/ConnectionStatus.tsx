import React, { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { sayHello } from 'reduxes/base/base.actions';
import { selectHello, selectUUID } from 'reduxes/base/base.selector';

const ConnectionStatus: FC = () => {
  const dispatch = useDispatch();
  const hello = useSelector(selectHello);
  const UUID = useSelector(selectUUID);
  return (
    <div>
      heheheheh
      <div>{hello}</div>
      <div>your UUID: {UUID}</div>
      <button
        onClick={() => {
          dispatch(sayHello());
        }}
      >
        SAY HELLO!
      </button>
    </div>
  );
};

export default ConnectionStatus;
