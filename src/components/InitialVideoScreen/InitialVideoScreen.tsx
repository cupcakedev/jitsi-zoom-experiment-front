import React, { FC, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DeviceSelector from 'components/DeviceSelector/DeviceSelector';
import { setVideoDeviceId, setAudioDeviceId } from 'reduxes/base/base.actions';
import * as Styled from './style';
import { selectBase } from 'reduxes/base/base.selector';
import { VideoBox } from 'components/VideoBox/VideoBox';

const InitialVideoScreen: FC = () => {
  const [videoBox, setVideoBox] = useState({
    audioTrack: undefined,
    videoTrack: undefined,
  });
  const dispatch = useDispatch();
  const base = useSelector(selectBase);

  const boxWidthAndHeight = {
    height: 350,
    width: 500,
  };

  const updateLocalTracks = () => {
    removeExistedLocalTracks()
      .then(() => {
        return JitsiMeetJS.createLocalTracks({
          devices: ['audio', 'video'],
          cameraDeviceId: base.cameraDeviceId,
          micDeviceId: base.micDeviceId,
        });
      })
      .then((tracks: any[]) => {
        const [audioTrack, videoTrack] = tracks;
        setVideoBox({
          audioTrack,
          videoTrack,
        });
      });
  };

  useEffect(() => {
    updateLocalTracks();
  }, [base.cameraDeviceId, base.micDeviceId]);

  const removeExistedLocalTracks = () => {
    const { audioTrack, videoTrack } = videoBox;

    const promises = [audioTrack, videoTrack]
      .filter((track) => !!track)
      .map((track: any) => {
        return new Promise((resolve) => {
          track.dispose().then(resolve);
        });
      });

    return Promise.all(promises);
  };

  const onAudioChange = (device: any) => {
    if (device) {
      dispatch(setAudioDeviceId(device.deviceId));
    } else {
      dispatch(setAudioDeviceId(null));
    }
  };

  const onVideoChange = (device: any) => {
    if (device) {
      dispatch(setVideoDeviceId(device.deviceId));
    } else {
      dispatch(setVideoDeviceId(null));
    }
  };

  return (
    <Styled.Root>
      <Styled.Title>Hello There</Styled.Title>
      <Styled.Content>
        <div>
          <VideoBox boxWidthAndHeight={boxWidthAndHeight} />
        </div>
      </Styled.Content>
      <Styled.AudioVideoInputsContainer>
        <Styled.SelectGroup>
          <Styled.SelectLabel>Video input:</Styled.SelectLabel>
          <DeviceSelector
            type="video"
            onChange={onVideoChange}
            value={base.cameraDeviceId}
          />
        </Styled.SelectGroup>
        <Styled.SelectGroup>
          <Styled.SelectLabel>Audio input:</Styled.SelectLabel>
          <DeviceSelector
            type="audio"
            onChange={onAudioChange}
            value={base.micDeviceId}
          />
        </Styled.SelectGroup>
      </Styled.AudioVideoInputsContainer>
    </Styled.Root>
  );
};

export default InitialVideoScreen;
