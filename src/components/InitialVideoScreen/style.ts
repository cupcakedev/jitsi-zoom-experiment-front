import styled from 'styled-components';

const Root = styled.div`
  width: 600px;
  background-color: pink;
  padding 10px;
  margin: auto;
`;

const Title = styled.h2`
  margin-top: 0;
  margin-bottom: 10px;
  text-align: center;
`;

const ActionContainer = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 10px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: row;

  & > div {
    flex: 1;
    align-items: center;
    justify-content: flex-start;
    display: flex;
    flex-direction: column;

    & > * {
      width: 100%;
    }

    &:last-child {
      padding-left: 10px;
      box-sizing: border-box;
    }
  }
`;

const InputsContainer = styled.div`
  padding-top: 10px;
`;

const AudioVideoInputsContainer = styled.div`
  display: flex;
  align-items: center;
`;

const SelectGroup = styled.div`
  margin-top: 15px;
  width: calc(50% - 5px);

  & + & {
    margin-left: 10px;
  }
`;

const SelectLabel = styled.div`
  font-size: 14px;
  padding-bottom: 10px;
  box-sizing: border-box;
`;

const BrowserWarning = styled.div`
  font-weight: bold;
  padding: 1rem 0.5rem;
  text-align: center;
  font-size: 20px;
`;

export {
  Root,
  Title,
  ActionContainer,
  Content,
  InputsContainer,
  SelectGroup,
  SelectLabel,
  BrowserWarning,
  AudioVideoInputsContainer,
};
