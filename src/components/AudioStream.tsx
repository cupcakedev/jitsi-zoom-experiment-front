import React, { FC, memo, useEffect, useRef } from 'react';

interface Props {
  audioTrack: any;
  isUser: boolean;
}

const AudioStream: FC<Props> = memo(({ audioTrack, isUser }) => {
  const audioRef = useRef<HTMLAudioElement>(null);

  useEffect(() => {
    const audio = audioRef.current;

    if (audioTrack && audio) {
      audioTrack.attach(audio);
    }
    return () => {
      if (audioTrack && audio) {
        audioTrack.detach(audio);
      }
    };
  }, [audioTrack, audioRef]);

  if (isUser) {
    return null;
  }

  return (
    <div style={{ visibility: 'collapse' }}>
      <audio autoPlay={true} ref={audioRef} />
    </div>
  );
});

AudioStream.displayName = 'AudioStream';
export default AudioStream;
