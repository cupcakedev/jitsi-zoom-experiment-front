import React, { FC, useContext, useRef } from 'react';
import * as SC from './styles';
import { MediaControls } from 'components/Controls/MediaControl';
import SpeakingHighlight from '../SpeakingHighlight/SpeakingHighlight';
import VideoStream from '../VideoStream/VideoStream';
import { PlayerContext } from 'contexts/PlayerContext';
import { useSelector } from 'react-redux';
import {
  selectAudioTrack,
  selectMyJid,
  selectPlayerByJID,
  selectRTC,
  selectVideoTrack,
  selectZoomMediaStream,
  selectZoomParticipants,
  selectZoomUserId,
} from 'reduxes/conference/conference.selectors';
import ZoomVideo from '@zoom/videosdk';
import { useMemo } from 'react';
import { useEffect } from 'react';
import { VideoQuality } from '@zoom/videosdk';
import {
  Root as ZoomRoot,
  VideoRoot as ZoomVideoRoot,
} from '../VideoStream/styles';

interface Props {
  boxWidthAndHeight: any;
  id?: string;
}

const zoomLocalVideo = ZoomVideo.createLocalVideoTrack();

export const VideoBox: FC<Props> = ({ boxWidthAndHeight, id }) => {
  const { JID, name, UUID, video } = useContext(PlayerContext);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const userId = useSelector(selectZoomUserId(JID));
  const RTC = useSelector(selectRTC);
  const isZoom = useMemo(() => {
    return RTC.name === 'zoom';
  }, [RTC]);
  const mediaStream = useSelector(selectZoomMediaStream);
  const audioTrack = useSelector(selectAudioTrack(JID));
  const videoTrack = useSelector(selectVideoTrack(JID));
  const zoomParticipants = useSelector(selectZoomParticipants);
  const myJID = useSelector(selectMyJid);
  const thisPlayer = useSelector(selectPlayerByJID(myJID));
  const ZOOM_LOCAL_VIDEO = document.getElementById('zoom-local-video') as any;

  const itsMe = useMemo(() => {
    return myJID === JID;
  }, [myJID, JID]);

  useEffect(() => {
    if (isZoom && !video && JID === myJID) {
      mediaStream.startVideo();
    }
  }, []);

  useEffect(() => {
    if (isZoom && itsMe && zoomLocalVideo) {
      if (thisPlayer?.video) {
        zoomLocalVideo.start(ZOOM_LOCAL_VIDEO);
        console.log('local start');
      } else {
        zoomLocalVideo.stop();
        console.log('local stop');
      }
    }
  }, [isZoom, itsMe, ZOOM_LOCAL_VIDEO, thisPlayer?.video]);

  useEffect(() => {
    if (isZoom && mediaStream && canvasRef.current && userId && !itsMe) {
      if (video) {
        const render = async () => {
          try {
            await mediaStream.renderVideo(
              canvasRef.current,
              userId,
              600,
              400,
              0,
              0,
              VideoQuality.Video_360P,
            );
            console.log('render');
          } catch (e) {
            console.log('ERROROR');
            console.log(e);
          }
        };
        render();
      } else {
        const clearCanvas = async () => {
          await mediaStream.stopRenderVideo(canvasRef.current, userId);
          await mediaStream.clearVideoCanvas(canvasRef.current, {
            R: 128,
            G: 128,
            B: 128,
            A: 1,
          });
        };
        console.log('clear');
        clearCanvas();
      }
    }
    // const ctx = canvasRef.current?.getContext("2d");
    // ctx.fillStyle = "green";
    // ctx.fillRect(0, 0, 100, 100);

    // const canvascopy = canvasRef.current;
    // return () => {
    //   mediaStream?.stopRenderVideo(canvasRef.current, userId);
    // };
  }, [
    UUID,
    isZoom,
    mediaStream,
    video,
    zoomParticipants,
    userId,
    myJID,
    JID,
    itsMe,
  ]);

  return (
    <SC.Root boxWidthAndHeight={boxWidthAndHeight} id={id}>
      {isZoom ? (
        itsMe ? (
          <ZoomRoot>
            <ZoomVideoRoot id="zoom-local-video" muted={true} data-video="0" />
          </ZoomRoot>
        ) : (
          <SC.CanvasRoot>
            <SC.VideoCanvas width={600} height={400} ref={canvasRef} />
          </SC.CanvasRoot>
        )
      ) : (
        <VideoStream videoTrack={videoTrack} />
      )}
      <MediaControls video={isZoom && itsMe ? thisPlayer?.video : video} />
      <SC.NameBar>{name}</SC.NameBar>
      {audioTrack && <SpeakingHighlight audioTrack={audioTrack} />}
    </SC.Root>
  );
};
