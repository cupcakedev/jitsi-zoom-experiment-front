import styled, { keyframes } from 'styled-components';

export const shadowAnimation = keyframes`
  from {
    box-shadow: 0px 0px 0px 0px rgba(255,238,21,0.75);
  }

  to {
    box-shadow: 0px 0px 5px 6px rgba(255,238,21,0.75);
  }
`;

type RootProps = {
  boxWidthAndHeight: {
    width: number;
    height: number;
  } | null;
};

export const Root = styled.li<RootProps>`
  margin: 4px;
  position: relative;
  background-color: grey;
  box-sizing: border-box;
  display: flex;
  align-items: flex-end;
  height: ${(p) =>
    p.boxWidthAndHeight ? `${p.boxWidthAndHeight.height - 20}px` : 'auto'};
  width: ${(p) =>
    p.boxWidthAndHeight ? `${p.boxWidthAndHeight.width - 20}px` : 'auto'};
  z-index: 20;
  &._speaking {
    z-index: 20;
    animation: ${shadowAnimation} 5s linear forwards;
  }
`;

export const NameBar = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 100000;
  padding: 4px 3.3px;
  background-color: rgba(0, 0, 0, 0.6);
  width: 100%;
  color: white;
  font-size: 16px;
`;

export const CanvasRoot = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
`;

export const VideoCanvas = styled.canvas`
  width: 100%;
  height: 100%;
`;
