import React, { FC, useEffect, useMemo, useState } from 'react';
import VoiceAnalyzer from 'utils/VoiceAnalyzer';
import * as SC from './styles';

interface Props {
  audioTrack: any;
}

const SpeakingHighlight: FC<Props> = ({ audioTrack }) => {
  const [isSpeaking, setSpeaking] = useState(false);

  const analyzer = useMemo(() => {
    return new VoiceAnalyzer(setSpeaking);
  }, [setSpeaking]);

  useEffect(() => {
    if (audioTrack) {
      const originalStream = audioTrack.getOriginalStream();
      analyzer.update(originalStream || undefined);
    } else {
      analyzer.update(undefined);
    }
    return () => {
      analyzer.update(undefined);
    };
  }, [audioTrack, analyzer]);
  if (!isSpeaking) return null;
  return <SC.Root className={isSpeaking ? '_speaking' : ''} />;
};

export default SpeakingHighlight;
