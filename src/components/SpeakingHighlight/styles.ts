import styled, { keyframes } from 'styled-components';

const shadowAnimation = keyframes`
  from {
    box-shadow: 0px 0px 0px 0px rgba(255,238,21,0.75);
  }

  to {
    box-shadow: 0px 0px 2px 3px rgba(255,238,21,0.75);
  }
`;

export const Root = styled.div`
  height: 100%;
  width: 100%;
  z-index: 10;
  position: absolute;
  background-color: transparent;

  &._speaking {
    animation: ${shadowAnimation} 5s linear forwards;
  }
`;
