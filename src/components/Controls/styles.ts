import styled from 'styled-components';

export const ControlButton = styled.button`
  background: transparent;
  outline: none;
  border: none;
  color: white;
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  padding: 0;
  font-size: 29px;
  box-sizing: border-box;
  z-index: 50;
  cursor: pointer;
  transition: transform 0.2s ease;
`;

export const Root = styled.div`
  display: flex;
  position: absolute;
  z-index: 100000;
  padding: 4px 3.3px;
  background-color: rgba(0, 0, 0, 0.6);
`;
