import React, { FC, useCallback } from 'react';
import { MdVideocam, MdVideocamOff } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectMyJid,
  selectRTC,
} from 'reduxes/conference/conference.selectors';
import { Player, TrackTypes } from 'reduxes/conference/conference.types';
import * as conferenceActions from 'reduxes/conference/conference.actions';
import * as SC from './styles';

interface Props {
  JID: Player['JID'];
  status?: boolean;
}

export const VideoControl: FC<Props> = ({ JID, status }) => {
  const dispatch = useDispatch();
  const isMyControl = useSelector(selectMyJid) === JID;
  const isZoom = useSelector(selectRTC).name === 'zoom';
  const handleMuteClick = useCallback(() => {
    if (isMyControl) {
      dispatch(
        conferenceActions.toggleMuteMyself(
          TrackTypes.video,
          isZoom && isMyControl,
        ),
      );
    }
  }, [isMyControl, dispatch, isZoom]);

  return (
    <>
      {!status && (
        <SC.ControlButton onClick={handleMuteClick}>
          <MdVideocamOff style={{ fontSize: '15px', color: '#FFEB3B' }} />
        </SC.ControlButton>
      )}
      {status && (
        <SC.ControlButton onClick={handleMuteClick}>
          <MdVideocam style={{ fontSize: '15px', color: '#4CAF50' }} />
        </SC.ControlButton>
      )}
    </>
  );
};
