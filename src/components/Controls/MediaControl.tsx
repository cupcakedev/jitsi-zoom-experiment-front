import React, { FC, useContext } from 'react';
import * as SC from './styles';
import { AudioControl } from './AudioControl';
import { VideoControl } from './VideoControl';
import { PlayerContext } from 'contexts/PlayerContext';

interface Props {
  video: boolean;
}

export const MediaControls: FC<Props> = ({ video }) => {
  const { JID, audio } = useContext(PlayerContext);
  return (
    <SC.Root>
      <AudioControl JID={JID} status={audio} />
      <VideoControl JID={JID} status={video} />
    </SC.Root>
  );
};
