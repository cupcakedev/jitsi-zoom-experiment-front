import React, { FC, useCallback } from 'react';
import { MdMic, MdMicOff } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import { selectMyJid } from 'reduxes/conference/conference.selectors';
import { Player, TrackTypes } from 'reduxes/conference/conference.types';
import * as conferenceActions from 'reduxes/conference/conference.actions';
import * as SC from './styles';

interface Props {
  JID: Player['JID'];
  status?: boolean;
}

export const AudioControl: FC<Props> = ({ JID, status }) => {
  const dispatch = useDispatch();
  const isMyControl = useSelector(selectMyJid) === JID;
  const handleMuteClick = useCallback(() => {
    if (isMyControl) {
      dispatch(conferenceActions.toggleMuteMyself(TrackTypes.audio));
    }
  }, [isMyControl, dispatch]);

  return (
    <>
      {!status && (
        <SC.ControlButton onClick={handleMuteClick}>
          <MdMicOff style={{ fontSize: '15px', color: '#FFEB3B' }} />
        </SC.ControlButton>
      )}
      {status && (
        <SC.ControlButton onClick={handleMuteClick}>
          <MdMic style={{ fontSize: '15px', color: '#4CAF50' }} />
        </SC.ControlButton>
      )}
    </>
  );
};
