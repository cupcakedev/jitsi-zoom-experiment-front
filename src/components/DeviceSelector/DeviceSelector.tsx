import React, { FC, useState, useEffect } from 'react';
import Select from 'react-select';
import * as Styled from './style';

interface Props {
  type: 'audio' | 'video';
  onChange: (value: any) => void;
  value: string | null;
  disabled?: boolean;
}

const DeviceSelector: FC<Props> = ({ type, onChange, value, disabled }) => {
  const [userDevices, setUserDevices] = useState([] as MediaDeviceInfo[]);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    navigator.getUserMedia(
      { [type]: true },
      () => {
        navigator.mediaDevices.enumerateDevices().then((devices) => {
          setUserDevices(devices);
        });
      },
      () => {
        setErrorMessage('Error. Not enough device permissions.');
      },
    );
  }, [type]);

  const getOptions = () => {
    const deviceKind = type === 'audio' ? 'audioinput' : 'videoinput';

    const result = userDevices
      .filter((device) => device.kind === deviceKind)
      .map((device, index) => {
        const compiledName =
          device.kind === 'audioinput'
            ? `Microphone ${index}`
            : `Camera ${index}`;
        return {
          ...device.toJSON(),
          label: device.label || compiledName,
        };
      });
    return result;
  };

  const findDeviceById = (id: string | null) => {
    return userDevices.find((device) => device.deviceId === id);
  };

  return errorMessage ? (
    <Styled.ErrorMessage>{errorMessage}</Styled.ErrorMessage>
  ) : (
    <Select
      className="warp-select"
      classNamePrefix="warp-select"
      value={findDeviceById(value)}
      onChange={onChange}
      getOptionValue={(option) => option.deviceId}
      options={getOptions()}
      isDisabled={typeof disabled !== 'undefined' ? disabled : false}
    />
  );
};

export default DeviceSelector;
