import styled from 'styled-components';

const ErrorMessage = styled.div`
  font-size: 20px;
`;

export { ErrorMessage };
