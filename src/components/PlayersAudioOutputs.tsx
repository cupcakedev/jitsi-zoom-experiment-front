import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import AudioStream from './AudioStream';
import {
  selectMyJid,
  selectPlayersWithAudioTracks,
} from 'reduxes/conference/conference.selectors';

export const PlayersAudioOutput: FC = () => {
  const currentUserJitsiId = useSelector(selectMyJid);
  const playersWithAudioTracks = useSelector(selectPlayersWithAudioTracks);

  return (
    <div className="audio-output">
      {playersWithAudioTracks.map((player) => (
        <AudioStream
          key={player.JID}
          audioTrack={player.audioTrack ? player.audioTrack : undefined}
          isUser={player.JID === currentUserJitsiId}
        />
      ))}
    </div>
  );
};
