import { createContext } from 'react';
import { Player } from 'reduxes/conference/conference.types';

interface IPlayerContext {
  JID: Player['JID'];
  UUID?: Player['UUID'];
  name: Player['name'];
  audio?: Player['audio'];
  video?: Player['video'];
}

export const PlayerContext = createContext<IPlayerContext>({
  JID: 'JID',
  UUID: 'UUID',
  name: 'name',
  audio: true,
  video: true,
});
